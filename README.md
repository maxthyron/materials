# Lectures:

* [Web разработка](https://gitlab.com/maxthyron/materials/tree/master/web)
* [Защита информации](https://gitlab.com/maxthyron/materials/tree/master/def)
* [Экономика](https://gitlab.com/maxthyron/materials/tree/master/eco)
* [Философия](https://gitlab.com/maxthyron/materials/tree/master/phi)
* [Безопасность Жизнедеяетльности](https://gitlab.com/maxthyron/materials/tree/master/bzd)
* [Моделирование](https://gitlab.com/maxthyron/materials/tree/master/mod)
* [Тестирование](https://gitlab.com/maxthyron/materials/tree/master/tst)
* [Компьютерные сети](https://gitlab.com/maxthyron/materials/tree/master/net)
