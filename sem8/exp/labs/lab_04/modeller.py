import numpy.random as nr

class NormalGenerator:
    def __init__(self, _mu, _sigma):
        if _sigma < 0:
            raise ValueError("Параметр Sigma не должен быть отрицательным!")
        self.mu = _mu
        self.sigma = _sigma

    def next(self):
        return nr.uniform(self.mu, self.sigma)

class UniformGenerator:
    def __init__(self, _mu, _sigma):
        if _sigma < 0:
            raise ValueError("Параметр Sigma не должен быть отрицательным!")
        self.mu = _mu
        self.sigma = _sigma

    def next(self):
        return nr.uniform(self.mu, self.sigma)

class NormalGenerators:
    def __init__(self, _mu1, _sigma1, _mu2, _sigma2):
        if _sigma1 < 0:
            raise ValueError("Параметр Sigma не должен быть отрицательным!")
        if _sigma2 < 0:
            raise ValueError("Параметр Sigma не должен быть отрицательным!")
        self.mu1 = _mu1
        self.sigma1 = _sigma1
        self.mu2 = _mu2
        self.sigma2 = _sigma2

    def next(self):
        return nr.uniform(self.mu1, self.sigma1) if nr.rand() < 0.5 else nr.uniform(self.mu2, self.sigma2)


class RequestGenerator:
    def __init__(self, generator, t):
        self.type = t
        self._generator = generator
        self._receivers = set()

    def add_receiver(self, receiver):
        self._receivers.add(receiver)

    def remove_receiver(self, receiver):
        try:
            self._receivers.remove(receiver)
        except KeyError:
            pass

    def next_time_period(self):
        return self._generator.next()

    def emit_request(self):
        for receiver in self._receivers:
            receiver.receive_request(self.type)


class RequestProcessor(RequestGenerator):
    def __init__(self, generator1, generator2, reenter_probability=0):
#        super().__init__(generator, 0)
        self._generator1 = generator1
        self._generator2 = generator2
        self.queue = []
        self._current_queue_size = 0
        self._max_queue_size = 0
        self._processed_requests = 0
        self._reenter_probability = reenter_probability
        self._reentered_requests = 0

    @property
    def processed_requests(self):
        return self._processed_requests

    @property
    def max_queue_size(self):
        return self._max_queue_size

    @property
    def current_queue_size(self):
        return self._current_queue_size

    @property
    def reentered_requests(self):
        return self._reentered_requests

    def process(self):
        if self._current_queue_size > 0:
            self._processed_requests += 1
            self._current_queue_size -= 1
#            self.emit_request()
#            if nr.random_sample() < self._reenter_probability:
#                self._reentered_requests += 1
#                self.receive_request()

    def receive_request(self, t):
        self.queue.append(t)
        self._current_queue_size += 1
        if self._current_queue_size > self._max_queue_size:
            self._max_queue_size += 1

    def next_time_period(self):
        if len(self.queue) == 0:
            return 0
        genType = self.queue.pop(0)
        return self._generator1.next() if genType == 0 else self._generator2.next()


class Modeller:
    def __init__(self, gen1Mu, gen1Sigma, gen2Mu, gen2Sigma, sm1Mu, sm1Sigma, sm2Mu, sm2Sigma):
#        self._generator = RequestGenerator(NormalGenerators(gen1Mu, gen1Sigma, gen2Mu, gen2Sigma))
        self._generator1 = RequestGenerator(NormalGenerator(gen1Mu, gen1Sigma), 0)
        self._generator2 = RequestGenerator(NormalGenerator(gen2Mu, gen2Sigma), 1)
        self._processor = RequestProcessor(UniformGenerator(sm1Mu, sm1Sigma), UniformGenerator(sm2Mu, sm2Sigma))
        self._generator1.add_receiver(self._processor)
        self._generator2.add_receiver(self._processor)
#        self._generator.add_receiver(self._processor)

    def event_based_modelling(self, request_count):
        generator = self._generator
        processor = self._processor

        requestWaitPeriod = 0
        gen_period = generator.next_time_period()
        proc_period = gen_period + processor.next_time_period()
        while processor.processed_requests < request_count:
            if gen_period <= proc_period:
                generator.emit_request()
                gen_period += generator.next_time_period()
            if gen_period >= proc_period:
                processor.process()
                if processor.current_queue_size > 0:
                    processorNextTimePeriod = processor.next_time_period()
                    proc_period += processorNextTimePeriod
                    requestWaitPeriod += processorNextTimePeriod
                else:
                    proc_period = gen_period + processor.next_time_period()

        return (processor.processed_requests, processor.max_queue_size, proc_period, requestWaitPeriod/processor.processed_requests)

    def time_based_modelling(self, request_count, dt):
        generator1 = self._generator1
        generator2 = self._generator2
        processor = self._processor

        requestWaitPeriod = 0
        reqTimeQueue = []
        gen1_period = generator1.next_time_period()
        gen2_period = generator2.next_time_period()
        if gen1_period == min(gen1_period, gen2_period):
            generator1.emit_request()
        else:
            generator2.emit_request()
        proc_period = min(gen1_period, gen2_period) + processor.next_time_period()
        current_time = 0
        while processor.processed_requests < request_count:
            if gen1_period <= current_time:
                generator1.emit_request()
                reqTimeQueue.append(current_time)
                gen1_period += generator1.next_time_period()
            if gen2_period <= current_time:
                generator2.emit_request()
                reqTimeQueue.append(current_time)
                gen2_period += generator2.next_time_period()
            if current_time >= proc_period:
                processor.process()
                if len(reqTimeQueue) != 0:
                    requestWaitPeriod += (current_time - reqTimeQueue.pop(0))
                if processor.current_queue_size > 0:
                    processorNextTimePeriod = processor.next_time_period()
                    proc_period += processorNextTimePeriod
                    #requestWaitPeriod += processorNextTimePeriod
                else:
                    proc_period = min(gen1_period, gen2_period) + processor.next_time_period()
            current_time += dt

        return (processor.processed_requests, processor.max_queue_size, current_time, requestWaitPeriod/processor.processed_requests)
