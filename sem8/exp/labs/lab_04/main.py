from PyQt5 import uic, Qt 
from PyQt5.QtCore import pyqtSlot, Qt
from PyQt5.QtWidgets import QApplication, QWidget, QMessageBox, QMainWindow, QDialog, QGridLayout
from PyQt5.QtWidgets import QTableWidget,QTableWidgetItem
import sys
import modeller
import experiment


class MainWindow(QWidget):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.ui = uic.loadUi("window.ui", self)
        self.table = QDialog()
        self.tableLayout = QGridLayout()
        self.table.setLayout(self.tableLayout)

    @pyqtSlot(name='on_startExperiment_clicked')
    def _start_experiment(self):
        ui = self.ui
        gen1Intens = [   float(ui.lineEdit_generator_min_intens.text()),
                        float(ui.lineEdit_generator_max_intens.text()) ]
        gen1Sigma = [   float(ui.lineEdit_generator_min_sigma.text()),
                        float(ui.lineEdit_generator_max_sigma.text()) ]
        gen2Intens = [   float(ui.lineEdit_generator2_min_sigma.text()),
                        float(ui.lineEdit_generator2_max_sigma.text()) ]
        gen2Sigma = [   float(ui.lineEdit_generator2_min_sigma.text()),
                        float(ui.lineEdit_generator2_max_sigma.text()) ]
        smIntens1 = [   float(ui.lineEdit_servicemachine_min_intens.text()),
                        float(ui.lineEdit_servicemachine_max_intens.text()) ]
        smSigma1 = [   float(ui.lineEdit_servicemachine_min_sigma.text()),
                        float(ui.lineEdit_servicemachine_max_sigma.text()) ]
        smIntens2 = [   float(ui.lineEdit_servicemachine_min_intens_2.text()),
                        float(ui.lineEdit_servicemachine_max_intens_2.text()) ]
        smSigma2 = [   float(ui.lineEdit_servicemachine_min_sigma_2.text()),
                        float(ui.lineEdit_servicemachine_max_sigma_2.text()) ]

        ozkp = experiment.FactorialExperiment(gen1Intens, gen1Sigma, gen2Intens, gen2Sigma, smIntens1, smSigma1, smIntens2, smSigma2, 8)

        results = ozkp.start()

        for i in reversed(range(self.tableLayout.count())):
            self.tableLayout.itemAt(i).widget().setParent(None)

        self._show_table(results[6])
        self._show_results_experiment(results);

    def _show_table(self, data):
        self.table.setGeometry(1000, 1000, 1100, 1000)
        self.table.show()

        columns = data[0]
        mat = data[1]

        table = QTableWidget(self)
        table.setColumnCount(len(columns))
        table.setRowCount(len(mat))

        table.setHorizontalHeaderLabels(columns)
        for i in range(len(columns)):
            table.horizontalHeaderItem(i).setTextAlignment(Qt.AlignCenter)

        for i in range(len(mat)):
            for j in range(len(columns)):
                table.setItem(i, j, QTableWidgetItem(str(mat[i][j])))

        table.resizeColumnsToContents()
        table.setGeometry(1000, 1000, 1100, 1000)

        self.tableLayout.addWidget(table)

    def _show_results_experiment(self, results):
        ui = self.ui
        ui.nonLinCoeffsEdit.setText(results[2])

        y = results[0][0]
        nonLinReg = results[1][0]

        print(y, nonLinReg)

        comparison = "ОЦКП\n"
        comparison += "Звездное плечо Alpha = {:.4f}\n".format(results[5])
        comparison += "Частично нелинейный план:\n"+results[4]+"\n\n"
        comparison += "Среднее время ожидания заявок: {:.4f}\n".format(y)
        comparison += "В частично нелинейном плане: {:.4f}\n\n".format(nonLinReg)

        comparison += "Отклонение частично нелинейного плана: {:.1f}%\n\n".format(100 - min(nonLinReg, y)*100/max(nonLinReg, y))
        
        ui.resultsEdit.setText(results[3]+"\n\n"+comparison)


def main():
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    return app.exec()


if __name__ == '__main__':
    sys.exit(main())
