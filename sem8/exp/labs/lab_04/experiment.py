from math import sqrt
import pyDOE2 as doe
import numpy as np
import modeller
import sys

"""

fac - factorials amount

"""

class FactorialExperiment:
    def __init__(self, gen1Intens, gen1Disp, gen2Intens, gen2Disp, sm1Intens, sm1Disp, sm2Intens,  sm2Disp, fac=8):
        self.fac = fac
        self.x1 = gen1Intens
        self.x2 = gen1Disp
        self.x3 = gen2Intens
        self.x4 = gen2Disp
        self.x5 = sm1Intens
        self.x6 = sm1Disp
        self.x7 = sm2Intens
        self.x8 = sm2Disp
        self.N0 = 2**self.fac
        self.N = 2**self.fac + 2*self.fac + 1
        self.a = sqrt(self.N0/self.N)
        self.alpha = sqrt((1/2) * (sqrt(self.N*self.N0) - self.N0))

        self.mat = []

        self.mat = doe.fracfact("a b c d e f g h ab ac ad ae af ag ah bc bd be bf bg bh cd ce cf cg ch de df dg dh ef eg eh fg fh gh")
        self.mat = np.insert(self.mat, 0, 1, axis=1)

    def updateMatrix(self):
        p1 = np.full((self.fac*2, 1), 1)
        p2 = np.zeros((self.fac*2, len(self.mat[0])-1))
        p3 = np.full((self.fac*2, self.fac), -self.a)

        j = 0
        for i in range(0, len(p2), 2):
            p2[i][j] = -self.alpha
            p2[i+1][j] = self.alpha
            j += 1

        j = 0
        for i in range(0, len(p3), 2):
            p3[i][j] = self.alpha**2 - self.a
            p3[i+1][j] = self.alpha**2 - self.a
            j += 1

        p12 = np.append(p1, p2, axis=1)
        p123 = np.append(p12, p3, axis=1)

        f1 = np.zeros((1, len(self.mat[0])))
        f1[0][0]=1
        f2 = np.full((1, self.fac), -self.a)
        f12 = np.append(f1, f2, axis=1)

        add0 = np.append(p123, f12, axis=0)

        add1 = np.full((self.N0, self.fac), 1 - self.a)
        self.mat = np.append(self.mat, add1, axis=1)
        self.mat = np.append(self.mat, add0, axis=0)

    def getY(self, mat):
        # Get y for smo
        y = []
        factors = []
        for i in mat:
            x1 = max(self.x1) if i[1] > 0 else min(self.x1)
            x2 = max(self.x2) if i[2] > 0 else min(self.x2)
            x3 = max(self.x3) if i[3] > 0 else min(self.x3)
            x4 = max(self.x4) if i[4] > 0 else min(self.x4)
            x5 = max(self.x5) if i[5] > 0 else min(self.x5)
            x6 = max(self.x6) if i[6] > 0 else min(self.x6)
            x7 = max(self.x7) if i[7] > 0 else min(self.x7)
            x8 = max(self.x8) if i[8] > 0 else min(self.x8)

            x1Time = 1/x1
            x2Time = sqrt(x2)
            x3Time = 1/x3
            x4Time = sqrt(x4)
            x5Time = 1/x5
            x6Time = sqrt(x6)
            x7Time = 1/x7
            x8Time = sqrt(x8)

            x1Time *= abs(i[1])
            x2Time *= abs(i[2])
            x3Time *= abs(i[3])
            x4Time *= abs(i[4])
            x5Time *= abs(i[5])
            x6Time *= abs(i[6])
            x7Time *= abs(i[7])
            x8Time *= abs(i[8])

            model = modeller.Modeller(x1Time, x2Time, x3Time, x4Time, x5Time, x6Time, x7Time, x8Time)
            res = model.time_based_modelling(1000, 0.1)
            factors.append([(x1, x2), (x3, x4), (x5, x6), (x7, x8)])
            
            y.append(res[3])

        return y, factors

    def getRegressionKoeffs(self, mat, y):
        # Get regression's coeffs
        k = []
        for j in range(len(mat[0])):
            s = 0
            ss = 0
            for i in range(len(mat)):
                s += mat[i][j]*y[i]
                ss += mat[i][j]**2
            k.append(s/ss)

        return k

    def getTextData(self, k):
        koeffs = ""
        start2 = 1
        k1 = 0
        k2 = 0
        for i in range(len(k)-self.fac):
            if k1 == 0:
                koeffs += "b{:} = {:.4f}\n".format(k2, k[i])
            else:
                koeffs += "b{:} = {:.4f}\n".format(k1*10+k2, k[i])
            if k2 % self.fac == 0 and k2 != 0:
                k1 += 1
                start2 += 1
                k2 = start2
                continue
            k2 += 1
        print(koeffs)

        k2 = self.fac
        for i in range(len(k)-self.fac, len(k)):
            k2 += 1
            koeffs += "b_a{:} = {:.4f}\n".format(k2, k[i])

        formula = "y = "
        start2 = 1
        k1 = 0
        k2 = 0
        for i in range(len(k)-self.fac):
            if (i < len(k) and i != 0):
                formula += " + " if k[i] > 0 else " - "
            if i == 0 and k[i] < 0:
                formula += " - "
            if k1 == 0:
                formula += "{:.4f}*x{:}".format(abs(k[i]), k2)
            else:
                formula += "{:.4f}*x{:}*x{:}".format(abs(k[i]), k1, k2)
            if k2 % self.fac == 0 and k2 != 0:
                k1 += 1
                start2 += 1
                k2 = start2
                continue
            k2 += 1

        k2 = 1
        for i in range(len(k)-self.fac, len(k)):
            if (i < len(k)):
                formula += " + " if k[i] > 0 else " - "
            formula += "{:.4f}*(x{:}^2-{:.2f})".format(abs(k[i]), k2, self.alpha)
            k2 += 1

        formula +=  "\n\ny = "
        start2 = 1
        k1 = 0
        k2 = 0
        for i in range(len(k)-self.fac):
            if (i < len(k) and i != 0):
                formula += " + " if k[i] > 0 else " - "
            if i == 0 and k[i] < 0:
                formula += " - "
            if k1 == 0:
                formula += "{:.4f}*x{:}".format(abs(k[i]), k2)
            else:
                formula += "{:.4f}*x{:}*x{:}".format(abs(k[i]), k1, k2)
            if k2 % self.fac == 0 and k2 != 0:
                k1 += 1
                start2 += 1
                k2 = start2
                continue
            k2 += 1

        alphaMult = 0
        k2 = 1
        for i in range(len(k)-self.fac, len(k)):
            if (i < len(k)):
                formula += " + " if k[i] > 0 else " - "
            formula += "{:.4f}*x{:}^2".format(abs(k[i]), k2)
            alphaMult += k[i] * (-self.alpha)
            k2 += 1
        formula += " + " if alphaMult > 0 else " - "
        formula += "{:.4f}".format(abs(alphaMult))

        return koeffs, formula

    def calculateRegression(self, mat, k):
        yRegression = []
        for i in range(len(mat)):
            s = 0
            for j in range(len(k)):
                s += mat[i][j] * k[j]
            yRegression.append(s)

        return yRegression

    def getExperiments(self, yRegression, y, factors):
        experiments = ""
        for i in range(len(y)):
            experiments += "Эксперимент {:}:\n".format(i+1)
            experiments += "Параметры генератора1(x1, x2)(a, b): {:}\n".format(factors[i][0])
            experiments += "Параметры генератора2(x3, x4)(a, b): {:}\n".format(factors[i][1])
            experiments += "Параметры ОА1(x5, x6)(a, b): {:}\n".format(factors[i][2])
            experiments += "Параметры ОА2(x7, x8)(a, b): {:}\n".format(factors[i][3])
            experiments += "Ожидаемый результат: {:.4f}; Полученный результат: {:.4f}\n\n".format(yRegression[i], y[i])

        return experiments

    def getPipeline(self, mat, y, factors):
        k = self.getRegressionKoeffs(mat, y)
        textData = self.getTextData(k)
        yRegression = self.calculateRegression(mat, k)
        experiments = self.getExperiments(yRegression, y, factors)

        return y, yRegression, textData[0], experiments, textData[1], self.alpha

    def showMatrix(self, mat, y):

        y = np.array(y)
        y = np.resize(y, (self.N, 1))
        mat = np.append(mat, y, axis=1)
        mat = np.around(mat, decimals=2)

        data = mat

        columns = []

        koeffs = ""
        start2 = 1
        k1 = 0
        k2 = 0
        for i in range(len(self.mat[0])-self.fac):
            if k1 == 0:
                columns.append("x{:}".format(k2))
            else:
                columns.append("x{:}x{:}".format(k1, k2))
            if k2 % self.fac == 0 and k2 != 0:
                k1 += 1
                start2 += 1
                k2 = start2
                continue
            k2 += 1
        for i in range(self.fac):
            columns.append("x{:}^2-a".format(i+1))
        columns.append("y")

        return [columns, mat]


    def start(self):
        if len(self.mat) == 0:
            return None

        self.updateMatrix()
        y, factors = self.getY(self.mat)
        res = self.getPipeline(self.mat, y, factors)

        data = self.showMatrix(self.mat, y)
        res += (data,)

        return res

if __name__ == '__main__':
    sys.exit(main())
