import numpy as np
import matplotlib.pyplot as plt
import math
import sys
from PyQt5 import uic

from PyQt5.QtGui import QTextCursor
from PyQt5.QtWidgets import (QWidget, QMainWindow, QLineEdit,
                             QLabel, QApplication, QComboBox, QMessageBox)


class UniformProcessor:
    def __init__(self, A, B):
        self.attached_to = None
        self.a = A
        self.b = B
        self.working = False

    def process(self):
        time = np.random.uniform(self.a, self.b)
        return time


class UniformGenerator:
    def __init__(self, A, B):
        self.attached_to = None
        self.a = A
        self.b = B


    def generate(self):
        time = np.random.uniform(self.a, self.b)
        return time



def main(gi, pi, gd, pd, count):
    mas1 = []
    mas2 = []
    proceeded = 0
    generated = 0
    to_proceed = count

    generator = UniformGenerator(gi, gd)
    processor = UniformProcessor(pi, pd)
    queue = []
    gtime = generator.generate()
    ptime = generator.generate() + processor.process()

    average_gen = 0
    average_time = 0
    average_proc = 0
    temp = 0

    while proceeded < to_proceed:
        if gtime <= ptime:
            queue.append(gtime)
            print("t = %8.4f \t Сгенерирована заявка \t| Очередь: %d" % (gtime, len(queue)))
            g = generator.generate()
            gtime += g
            average_gen += g
            generated += 1
        if gtime >= ptime:
            if len(queue) > 0:
                time = queue[0]
                average_time += (ptime - time)
                queue = queue[1:]
                proceeded += 1
                print(
                    "t = %8.4f \t Обработана заявка    \t| Очередь: %d (прибыла в очередь в %8.4f, время пребыввания: %8.4f)" % (
                    ptime, len(queue), time, ptime - time))
                if len(queue) > 0:
                    p = processor.process()
                    ptime += p
                    average_proc += p
                else:
                    p = processor.process()
                    ptime = gtime + p
                    average_proc += p
            else:
                ptime = gtime + processor.process()

        if temp < average_time:
            temp = average_time
            average_time /= proceeded
            average_proc /= proceeded
            average_gen /= generated
            intensity_gen = 1 / average_gen
            intensity_proc = 1 / average_proc
            load = average_proc / average_time

            mas1.append(average_time)
            mas2.append(load)

    average_time /= proceeded
    average_proc /= proceeded
    average_gen /= generated
    intensity_gen = 1 / average_gen
    intensity_proc = 1 / average_proc
    load = intensity_gen / intensity_proc
    # print("Среднее время пребывания: " + str(average_time))
    # print("Среднее время поступления: " + str(average_gen))
    # print("Среднее время обработки: " + str(average_proc))
    # print("Интенсивность поступления: " + str(intensity_gen))
    # print("Интенсивность обслуживания: " + str(intensity_proc))
    # print("Загрузка: "+str(load))


    return [average_time, average_proc, average_gen, intensity_gen, intensity_proc, load, mas1, mas2], \
           "Среднее время пребывания:  \t %.4f" % (average_time) + "\nСреднее время поступления: \t %.4f" % (
               average_gen) + "\n" \
                              "Среднее время обработки:   \t %.4f" % (average_proc) + "\n" \
                                                                                      "Интенсивность поступления: \t %.4f" % (
               intensity_gen) + "\n" \
                                "Интенсивность обслуживания:\t %.4f" % (intensity_proc) + "\n" \
                                                                                          "Загрузка:                  \t\t %.4f" % (
               load)


class MainWindow(QMainWindow):
    def __init__(self):
        QWidget.__init__(self)
        uic.loadUi("mainwindow.ui", self)
        self.initUI()

    def initUI(self):
        self.ResualtButton.clicked.connect(self.calculate)

        self.GeneratorI.setText("0")
        self.GeneratorD.setText("5")

        self.ProcessorI.setText("1")
        self.ProcessorD.setText("3")

        self.Task.setText("1000")

        self.pushButton.clicked.connect(self.experiment)

        self.changable.addItems(
            ['Интенсивность ОА', 'Интенсивность генератора', 'Дисперсия ОА', 'Дисперсия генератора', 'Время ожидания'])

        self.ret.addItems(
            ['Загрузка', 'Среднее время обработки', 'Среднее время пребывания', 'Среднее время поступления'])

        self.StepLabel.setText("0.1")

        self.show()

    def calculate(self):
        try:
            gen_i = float(self.GeneratorI.text())
            gen_d = float(self.GeneratorD.text())
            proc_i = float(self.ProcessorI.text())
            proc_d = float(self.ProcessorD.text())
            count = int(self.Task.text())

            res = main(gen_i, proc_i, gen_d, proc_d, count)
            print(res[1])

            self.Result.setText(res[1])
            self.Result.adjustSize()
            self.update()
        except ValueError:
            QMessageBox.warning(self, 'Ошибка', 'Ошибка в данных!')
        except Exception as e:
            QMessageBox.critical(self, 'Ошибка', e)

    def experiment(self):
        try:
            gen_i = float(self.GeneratorI.text())
            gen_d = float(self.GeneratorD.text())
            proc_i = float(self.ProcessorI.text())
            proc_d = float(self.ProcessorD.text())
            count = int(self.Task.text())
            if gen_d == 0:
                gen_d = 0.00000001

            params = [gen_i, gen_d, proc_i, proc_d, count]

            param_num = 5
            res_num = 3

            if self.changable.currentText() == 'Интенсивность ОА':
                param_num = 2
            elif self.changable.currentText() == 'Интенсивность генератора':
                param_num = 0
            elif self.changable.currentText() == 'Дисперсия ОА':
                param_num = 3
            elif self.changable.currentText() == 'Дисперсия генератора':
                param_num = 1
            elif self.changable.currentText() == 'Время ожидания':
                param_num = 6


            if self.ret.currentText() == 'Загрузка':
                res_num = 5
            elif self.ret.currentText() == 'Среднее время обработки':
                res_num = 1
            elif self.ret.currentText() == 'Среднее время пребывания':
                res_num = 0
            elif self.ret.currentText() == 'Среднее время поступления':
                res_num = 2

            step = float(self.StepLabel.text())
            exp_count = 20

            x = []
            y = []
            if param_num == 6:
                res = main(params[0], params[2], params[1], params[3], params[4])
                y.append(res[0][param_num])
                x.append(res[0][param_num + 1])
                x[0].sort()
                y[0].sort()
                print(x, "\n\n", y)
                plt.plot(x[0], y[0], '-')
                plt.xlabel('Загрузка', fontsize=16)
                plt.ylabel('Время', fontsize=16)
                plt.grid(True)
                plt.show()
            else:
                for i in range(exp_count):
                    x.append(params[param_num])
                    res = main(params[0], params[2], params[1], params[3], params[4])
                    y.append(res[0][res_num])
                    params[param_num] += step

                plt.plot(x, y, '-')
                plt.grid(True)
                plt.show()

        except ValueError:
            QMessageBox.warning(self, 'Ошибка', 'Ошибка в данных!')
        except Exception as e:
            QMessageBox.critical(self, 'Ошибка', e)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    mw = MainWindow()
    mw.show()
    sys.exit(app.exec_())
