import matplotlib.pyplot as plt
from math import sqrt
import pyDOE2 as doe
import numpy as np
import modeller
import sys

"""

fac - factorials amount
modde - leniar or non-leniar experiment

"""


class FactorialExperiment:
    def __init__(self, gen1Intens, gen1Sigma, gen2Intens, gen2Sigma, smIntens1, smSigma1, smIntens2, smSigma2, fac=4,
                 mode=0):
        self.mode = mode
        self.fac = fac
        self.x1 = gen1Intens
        self.x2 = gen1Sigma
        self.x3 = gen2Intens
        self.x4 = gen2Sigma
        self.x5 = smIntens1
        self.x6 = smSigma1
        self.x7 = smIntens2
        self.x8 = smSigma2

        self.matLin = [];
        self.matNonLin = [];

        if self.mode == 0:
            self.matLin = doe.fracfact("a b c d e f g h")
            self.matLin = np.insert(self.matLin, 0, 1, axis=1)
            self.matNonLin = doe.fracfact(
                "a b c d e f g h ab ac ad ae af ag ah bc bd be bf bg bh cd ce cf cg ch de df dg dh ef eg eh fg fh gh")
            self.matNonLin = np.insert(self.matNonLin, 0, 1, axis=1)
            print('Mod 0')
        elif self.mode == 1:
            self.matLin = doe.fracfact("a b c d e f g abcdefg")
            print('Mod 1 str 1')
            self.matLin = np.insert(self.matLin, 0, 1, axis=1)
            print('Mod 1 str 2')
            self.matNonLin = doe.fracfact(
                "a b c d e f g abcdefg ab ac ad ae af ag aabcdefg bc bd be bf bg babcdefg cd ce cf cg cabcdefg de df dg dabcdefg ef eg eabcdefg fg fabcdefg gabcdefg")
            print('Mod 1 str 3')
            self.matNonLin = np.insert(self.matNonLin, 0, 1, axis=1)
            print('Mod 1')
    def getY(self, mat):
        # Get y for smo
        y = []
        factors = []
        for i in mat:
            print(i)
            x1 = max(self.x1) if i[1] > 0 else min(self.x1)
            x2 = max(self.x2) if i[2] > 0 else min(self.x2)
            x3 = max(self.x3) if i[3] > 0 else min(self.x3)
            x4 = max(self.x4) if i[4] > 0 else min(self.x4)
            x5 = max(self.x5) if i[5] > 0 else min(self.x5)
            x6 = max(self.x6) if i[6] > 0 else min(self.x6)
            x7 = max(self.x7) if i[7] > 0 else min(self.x7)
            x8 = max(self.x8) if i[8] > 0 else min(self.x8)

            x1Time = 1 / x1
            x2Time = sqrt(x2)
            x3Time = 1 / x3
            x4Time = sqrt(x4)
            x5Time = 1 / x5
            x6Time = sqrt(x6)
            x7Time = 1 / x7
            x8Time = sqrt(x8)

            if self.mode == 0:
                model = modeller.Modeller(x1Time, x2Time, x3Time, x4Time, x5Time, x6Time, x7Time, x8Time)
                factors.append([(x1, x2), (x3, x4), (x5, x6), (x7, x8)])
            else:
                model = modeller.Modeller(x1Time, x2Time, x3Time, x4Time, x5Time,
                                          x1Time * x2Time * x3Time * x4Time * x5Time, x7Time,
                                          x1Time * x2Time * x3Time * x4Time * x7Time)
                factors.append([(x1, x2), (x3, x4), (x5, x1 * x2 * x3 * x4 * x5), (x7, x1 * x2 * x3 * x4 * x7)])
            res = model.event_based_modelling(1000)

            y.append(res[3])

        return y, factors

    def getRegressionKoeffs(self, mat, y):
        # Get regression's coeffs
        k = []
        for j in range(len(mat[0])):
            s = 0
            for i in range(len(mat)):
                s += mat[i][j] * y[i]
            k.append(s / (2 ** self.fac))

        return k

    def getTextData(self, k):
        koeffs = ""
        start2 = 1
        k1 = 0
        k2 = 0
        for i in range(len(k)):
            if k1 == 0:
                koeffs += "b{:} = {:.4f}\n".format(k2, k[i])
            else:
                koeffs += "b{:} = {:.4f}\n".format(k1 * 10 + k2, k[i])
            if k2 % self.fac == 0 and k2 != 0:
                k1 += 1
                start2 += 1
                k2 = start2
                continue
            k2 += 1

        formula = "y = "
        start2 = 1
        k1 = 0
        k2 = 0
        for i in range(len(k)):
            if (i < len(k) and i != 0):
                formula += " + " if k[i] > 0 else " - "
            if i == 0 and k[i] < 0:
                formula += " - "
            if k1 == 0:
                formula += "{:.4f}*x{:}".format(abs(k[i]), k2)
            else:
                formula += "{:.4f}*x{:}".format(abs(k[i]), k1 * 10 + k2)
            if k2 % self.fac == 0 and k2 != 0:
                k1 += 1
                start2 += 1
                k2 = start2
                continue
            k2 += 1

        return koeffs, formula

    def calculateRegression(self, mat, k):
        yRegression = []
        for i in range(len(mat)):
            s = 0
            for j in range(len(k)):
                s += mat[i][j] * k[j]
            yRegression.append(s)

        return yRegression

    def getExperiments(self, yRegression, y, factors):
        experiments = ""
        for i in range(len(y)):
            experiments += "Эксперимент {:}:\n".format(i + 1)
            experiments += "Параметры генератора1(x1, x2)(1/mu, sigma^2): {:}\n".format(factors[i][0])
            experiments += "Параметры генератора2(x3, x4)(1/mu, sigma^2): {:}\n".format(factors[i][1])
            experiments += "Параметры ОА(x5, x6)(1/mu, sigma^2): {:}\n".format(factors[i][2])
            experiments += "Ожидаемый результат: {:.4f}; Полученный результат: {:.4f}\n\n".format(yRegression[i], y[i])

        return experiments

    def getPipeline(self, mat, y, factors):
        k = self.getRegressionKoeffs(mat, y)
        textData = self.getTextData(k)
        yRegression = self.calculateRegression(mat, k)
        experiments = self.getExperiments(yRegression, y, factors)

        return y, yRegression, textData[0], experiments, textData[1]

    def showMatrix(self, data):

        columns = ('Freeze', 'Wind', 'Flood', 'Quake', 'Hail')
        rows = ['%d year' % x for x in (100, 50, 20, 10, 5)]

        cell_text = np.round(data, 3)
        # row_labels = len(data)
        # col_labels = len(data[0])

        # kw = dict(cellColours=[["#EEECE1"] * len(data.iloc[0])] * len(data))  # to fill cells with color
        ytable = plt.table(cellText=cell_text, rowLabels=rows, colLabels=columns, loc="center right")
        plt.axis("off")
        plt.grid(False)
        plt.show()

    def start(self):
        print('yes')
        if len(self.matLin) == 0 or len(self.matNonLin) == 0:
            return None

        y, factors = self.getY(self.matLin)
        print('yes')
        res1 = self.getPipeline(self.matLin, y, factors)
        print('yes')
        res2 = self.getPipeline(self.matNonLin, y, factors)
        print('yes')
        return (res1, res2)


if __name__ == '__main__':
    sys.exit(main())
