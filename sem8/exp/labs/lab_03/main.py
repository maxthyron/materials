from PyQt5 import uic
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QApplication, QWidget, QMessageBox
import sys
import modeller
import experiment


class MainWindow(QWidget):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.ui = uic.loadUi("window.ui", self)

    @pyqtSlot(name='on_startExperiment_clicked')
    def _start_experiment(self):
        ui = self.ui
        gen1Intens = [float(ui.lineEdit_generator_min_intens.text()),
                      float(ui.lineEdit_generator_max_intens.text())]
        gen1Sigma = [float(ui.lineEdit_generator_min_sigma.text()),
                     float(ui.lineEdit_generator_max_sigma.text())]
        gen2Intens = [float(ui.lineEdit_generator_min_intens_2.text()),
                      float(ui.lineEdit_generator_max_intens_2.text())]
        gen2Sigma = [float(ui.lineEdit_generator_min_sigma_2.text()),
                     float(ui.lineEdit_generator_max_sigma_2.text())]
        smIntens1 = [float(ui.lineEdit_servicemachine_min_intens.text()),
                     float(ui.lineEdit_servicemachine_max_intens.text())]
        smSigma1 = [float(ui.lineEdit_servicemachine_min_sigma.text()),
                    float(ui.lineEdit_servicemachine_max_sigma.text())]
        smIntens2 = [float(ui.lineEdit_servicemachine_min_intens_2.text()),
                     float(ui.lineEdit_servicemachine_max_intens_2.text())]
        smSigma2 = [float(ui.lineEdit_servicemachine_min_sigma_2.text()),
                    float(ui.lineEdit_servicemachine_max_sigma_2.text())]

        pfe = experiment.FactorialExperiment(gen1Intens, gen1Sigma, gen2Intens, gen2Sigma, smIntens1, smSigma1,
                                             smIntens2, smSigma2, 8, 0)
        print('yes')
        dfe = experiment.FactorialExperiment(gen1Intens, gen1Sigma, gen2Intens, gen2Sigma, smIntens1, smSigma1,
                                             smIntens2, smSigma2, 8, 1)


        pfe_results = pfe.start()
        dfe_results = dfe.start()

        results = (pfe_results, dfe_results)

        self._show_results_experiment(results);

    def _show_results_experiment(self, results):
        pfe = results[0]
        dfe = results[1]
        ui = self.ui
        ui.linCoeffsEdit.setText(pfe[0][2])
        ui.nonLinCoeffsEdit.setText(pfe[1][2])
        ui.linCoeffsEdit_dfe.setText(dfe[0][2])
        ui.nonLinCoeffsEdit_dfe.setText(dfe[1][2])

        y = pfe[0][0][-1]
        linReg = pfe[0][1][-1]
        nonLinReg = pfe[1][1][-1]

        y_dfe = dfe[0][0][-1]
        linReg_dfe = dfe[0][1][-1]
        nonLinReg_dfe = dfe[1][1][-1]

        print(y, linReg)
        print(y, nonLinReg)

        comparison = "ПФЭ\n"
        comparison += "Линейный план\n" + pfe[0][4] + "\n"
        comparison += "Частично нелинейный план план\n" + pfe[1][4] + "\n\n"
        comparison += "Среднее время ожидания заявок: {:.4f}\n".format(y)
        comparison += "В линейном плане: {:.4f}\n".format(linReg)
        comparison += "В частично нелинейном плане: {:.4f}\n\n".format(nonLinReg)

        comparison += "Отклонение линейного плана: {:.1f}%\n".format(100 - min(linReg, y) * 100 / max(linReg, y))
        comparison += "Отклонение частично нелинейного плана: {:.1f}%\n\n".format(
            100 - min(nonLinReg, y) * 100 / max(nonLinReg, y))

        comparison += "ДФЭ\n"
        comparison += "Линейный план\n" + dfe[0][4] + "\n"
        comparison += "Частично нелинейный план\n" + dfe[1][4] + "\n\n"
        comparison += "Среднее время ожидания заявок: {:.4f}\n".format(y_dfe)
        comparison += "В линейном плане: {:.4f}\n".format(linReg_dfe)
        comparison += "В частично нелинейном плане: {:.4f}\n\n".format(nonLinReg_dfe)

        comparison += "Отклонение линейного плана: {:.1f}%\n".format(
            100 - min(linReg_dfe, y_dfe) * 100 / max(linReg_dfe, y_dfe))
        comparison += "Отклонение частично нелинейного плана: {:.1f}%\n\n".format(
            100 - min(nonLinReg_dfe, y_dfe) * 100 / max(nonLinReg_dfe, y_dfe))

        ui.resultsEdit.setText(pfe[1][3] + "\n\n" + dfe[1][3] + "\n\n" + comparison)


def main():
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    return app.exec()


if __name__ == '__main__':
    sys.exit(main())
