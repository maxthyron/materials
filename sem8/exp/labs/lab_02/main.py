from PyQt5 import uic
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QApplication, QWidget, QMessageBox
import sys
import modeller
import experiment


class MainWindow(QWidget):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.ui = uic.loadUi("window.ui", self)

    @pyqtSlot(name='on_startExperiment_clicked')
    def _start_experiment(self):
        ui = self.ui
        genIntens = [   float(ui.lineEdit_generator_min_intens.text()),
                        float(ui.lineEdit_generator_max_intens.text()) ]
        genSigma = [   float(ui.lineEdit_generator_min_sigma.text()),
                        float(ui.lineEdit_generator_max_sigma.text()) ]
        smIntens = [   float(ui.lineEdit_servicemachine_min_intens.text()),
                        float(ui.lineEdit_servicemachine_max_intens.text()) ]
        smSigma = [   float(ui.lineEdit_servicemachine_min_sigma.text()),
                        float(ui.lineEdit_servicemachine_max_sigma.text()) ]

        exper = experiment.FactorialExperiment(genIntens, genSigma, smIntens, smSigma, 4)

        results = exper.start()

        self._show_results_experiment(results);

    @pyqtSlot(name='on_pushButton_clicked')
    def _parse_parameters(self):
        try:
            ui = self.ui
            genMu = float(ui.lineEdit_generator_mu.text())
            genSigma = float(ui.lineEdit_generator_sigma.text())
            smMu = float(ui.lineEdit_servicemachine_mu.text())
            smSigma = float(ui.lineEdit_servicemachine_sigma.text())
            req_count = int(ui.lineEdit_request_count.text())
            method = ui.comboBox_method.currentIndex()

            model = modeller.Modeller(genMu, genSigma, smMu, smSigma)
            if method == 0:
                self._show_results(model.event_based_modelling(req_count))
            else:
                delta_t = float(ui.lineEdit_deltat.text())
                self._show_results(model.time_based_modelling(req_count, delta_t))
        except ValueError:
            QMessageBox.warning(self, 'Ошибка', 'Ошибка в данных!')
        except Exception as e:
            QMessageBox.critical(self, 'Ошибка', e)

    def _show_results_experiment(self, results):
        ui = self.ui
        ui.linCoeffsEdit.setText(results[0][2])
        ui.nonLinCoeffsEdit.setText(results[1][2])

        y = results[0][0][-1]
        linReg = results[0][1][-1]
        nonLinReg = results[1][1][-1]

        print(y, linReg)
        print("\narrrr\n")
        print(y, nonLinReg)

        comparison = ""
        comparison += "Среднее время ожидания заявок: {:.4f}\n".format(y)
        comparison += "В линейном плане: {:.4f}\n".format(linReg)
        comparison += "В частично нелинейном плане: {:.4f}\n".format(nonLinReg)

        comparison += "Отклонение линейного плана: {:.1f}%\n".format(100 - min(linReg, y)*100/max(linReg, y))
        comparison += "Отклонение частично нелинейного плана: {:.1f}%\n".format(100 - min(nonLinReg, y)*100/max(nonLinReg, y))
        
        ui.resultsEdit.setText(results[1][3]+"\n"+results[0][4]+"\n"+results[1][4]+"\n\n"+comparison)

    def _show_results(self, results):
        ui = self.ui
        ui.lineEdit_res_request_count.setText(str(results[0]))
        ui.lineEdit_res_max_queue_size.setText(str(results[1]))
        ui.lineEdit_res_time.setText('{:.2f}'.format(results[2]))
        ui.lineEdit_res_avarage_wait_time.setText("{:.2f}".format(results[3]))

    @pyqtSlot(int)
    def on_comboBox_method_currentIndexChanged(self, index):
        if index == 1:
            # Δt
            visibility = True
        else:
            visibility = False
            # events
        self.ui.lineEdit_deltat.setEnabled(visibility)


def main():
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    return app.exec()


if __name__ == '__main__':
    sys.exit(main())
