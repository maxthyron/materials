import numpy.random as nr


class UniformGenerator:
    def __init__(self, _mu, _sigma):
        if _sigma < 0:
            raise ValueError("Параметр Sigma не должен быть отрицательным!")
        self.mu = _mu
        self.sigma = _sigma

    def next(self):
        return nr.uniform(self.mu, self.sigma)

class RequestGenerator:
    def __init__(self, generator):
        self._generator = generator
        self._receivers = set()

    def add_receiver(self, receiver):
        self._receivers.add(receiver)

    def remove_receiver(self, receiver):
        try:
            self._receivers.remove(receiver)
        except KeyError:
            pass

    def next_time_period(self):
        return self._generator.next()

    def emit_request(self):
        for receiver in self._receivers:
            receiver.receive_request()


class RequestProcessor(RequestGenerator):
    def __init__(self, generator, reenter_probability=0):
        super().__init__(generator)
        self._generator = generator
        self._current_queue_size = 0
        self._max_queue_size = 0
        self._processed_requests = 0
        self._reenter_probability = reenter_probability
        self._reentered_requests = 0

    @property
    def processed_requests(self):
        return self._processed_requests

    @property
    def max_queue_size(self):
        return self._max_queue_size

    @property
    def current_queue_size(self):
        return self._current_queue_size

    @property
    def reentered_requests(self):
        return self._reentered_requests

    def process(self):
        if self._current_queue_size > 0:
            self._processed_requests += 1
            self._current_queue_size -= 1
            self.emit_request()
            if nr.random_sample() < self._reenter_probability:
                self._reentered_requests += 1
                self.receive_request()

    def receive_request(self):
        self._current_queue_size += 1
        if self._current_queue_size > self._max_queue_size:
            self._max_queue_size += 1

    def next_time_period(self):
        return self._generator.next()


class Modeller:
    def __init__(self, genMu, genSigma, smMu, smSigma):
        self._generator = RequestGenerator(UniformGenerator(genMu, genSigma))
        self._processor = RequestProcessor(UniformGenerator(smMu, smSigma))
        self._generator.add_receiver(self._processor)

    def event_based_modelling(self, request_count):
        generator = self._generator
        processor = self._processor

        requestWaitPeriod = 0
        gen_period = generator.next_time_period()
        proc_period = gen_period + processor.next_time_period()
        while processor.processed_requests < request_count:
            if gen_period <= proc_period:
                generator.emit_request()
                gen_period += generator.next_time_period()
            if gen_period >= proc_period:
                processor.process()
                if processor.current_queue_size > 0:
                    processorNextTimePeriod = processor.next_time_period()
                    proc_period += processorNextTimePeriod
                    requestWaitPeriod += processorNextTimePeriod
                else:
                    proc_period = gen_period + processor.next_time_period()

        return (processor.processed_requests, processor.max_queue_size, proc_period, requestWaitPeriod/processor.processed_requests)

    def time_based_modelling(self, request_count, dt):
        generator = self._generator
        processor = self._processor

        requestWaitPeriod = 0
        gen_period = generator.next_time_period()
        proc_period = gen_period + processor.next_time_period()
        current_time = 0
        while processor.processed_requests < request_count:
            if gen_period <= current_time:
                generator.emit_request()
                gen_period += generator.next_time_period()
            if current_time >= proc_period:
                processor.process()
                if processor.current_queue_size > 0:
                    processorNextTimePeriod = processor.next_time_period()
                    proc_period += processorNextTimePeriod
                    requestWaitPeriod += processorNextTimePeriod
                else:
                    proc_period = gen_period + processor.next_time_period()
            current_time += dt

        return (processor.processed_requests, processor.max_queue_size, current_time, requestWaitPeriod/processor.processed_requests)
