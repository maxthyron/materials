import pyDOE2 as doe
import numpy as np
import modeller
import sys

"""

fac - factorials amount
modde - leniar or non-leniar experiment

"""

class FactorialExperiment:
    def __init__(self, genIntens, genSigma, smIntens, smSigma, fac=4):
        self.fac = fac
        self.x1 = genIntens
        self.x2 = genSigma
        self.x3 = smIntens
        self.x4 = smSigma

        self.matLin = [];
        self.matNonLin = [];

        self.matLin = doe.fracfact("a b c d")
        self.matLin = np.insert(self.matLin, 0, 1, axis=1)
        self.matNonLin = doe.fracfact("a b c d ab ac ad bc bd cd abcd")
        self.matNonLin = np.insert(self.matNonLin, 0, 1, axis=1)

    def getY(self, mat):
        # Get y for smo
        y = []
        for i in mat:
            print(i)
            x1 = max(self.x1) if i[1] > 0 else min(self.x1)
            x2 = max(self.x2) if i[2] > 0 else min(self.x2)
            x3 = max(self.x3) if i[3] > 0 else min(self.x3)
            x4 = max(self.x4) if i[4] > 0 else min(self.x4)

            x1Time = 1/x1
            x3Time = 1/x3

            model = modeller.Modeller(x1Time, x2, x3Time, x4)
            res = model.event_based_modelling(1000)
            y.append(res[3])

        return y

    def getRegressionKoeffs(self, mat, y):
        # Get regression's coeffs
        k = []
        for j in range(len(mat[0])):
            s = 0
            for i in range(len(mat)):
                s += mat[i][j]*y[i]
            k.append(s/(2**self.fac))

        return k

    def getTextData(self, k):
        koeffs = ""
        for i in range(len(k)):
            if (i < 5):
                koeffs += "b{:} = {:.4f}\n".format(i, k[i])
            elif(i < 8):
                koeffs += "b1{:} = {:.4f}\n".format(i-3, k[i])
            elif(i < 10):
                koeffs += "b2{:} = {:.4f}\n".format(i-5, k[i])
            elif(i < 11):
                koeffs += "b34 = {:.4f}\n".format(k[i])
            else:
                koeffs += "b1234 = {:.4f}\n".format(k[i])

        formula = "y = "
        for i in range(len(k)):
            if (i < 5):
                formula += "{:.4f}*x{:}".format(k[i], i)
            elif(i < 8):
                formula += "{:.4f}*x1{:}".format(k[i], i-3)
            elif(i < 10):
                formula += "{:.4f}*x2{:}".format(k[i], i-5)
            elif(i < 11):
                formula += "{:.4f}*x34".format(k[i])
            else:
                formula += "{:.4f}*x1234".format(k[i])
            if (i < len(k) - 1):
                formula += " + " if k[i] > 0 else " - "

        return koeffs, formula

    def calculateRegression(self, mat, k):
        yRegression = []
        for i in range(len(mat)):
            s = 0
            for j in range(len(k)):
                s += mat[i][j] * k[j]
            yRegression.append(s)

        return yRegression

    def getExperiments(self, yRegression, y):
        experiments = ""
        for i in range(len(y)):
            experiments += "Эксперимент {:}:\n".format(i+1)
            experiments += "Ожидаемый результат: {:.4f}; Полученный результат: {:.4f}\n\n".format(yRegression[i], y[i])

        return experiments

    def getPipeline(self, mat, y):
        k = self.getRegressionKoeffs(mat, y)
        textData = self.getTextData(k)
        yRegression = self.calculateRegression(mat, k)
        experiments = self.getExperiments(yRegression, y)

        return y, yRegression, textData[0], experiments, textData[1]

    def start(self):
        if len(self.matLin) == 0 or len(self.matNonLin) == 0:
            return None

        y = self.getY(self.matLin)
        res1 = self.getPipeline(self.matLin, y)
        res2 = self.getPipeline(self.matNonLin, y)

        return (res1, res2)

if __name__ == '__main__':
    sys.exit(main())
