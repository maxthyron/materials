# Лекция 1. Экономическая оценка активов и пассивов.

Для осуществления предпринимательской деятельности необходимо пройти государственную регистрацию.  

Формы финансовой отчетности: 

1.  Бухгалтерский баланс. Характеризует финансовое состояние предприятия на какой-то период. 

 Активы - имущества предприятия: 

-   внеоборотные (ВА), используются более 12 месяцев: 
    -   нематериальные активы (патенты, изобретения, образцы, модели, авторское право, право на товарный знак); 
    -   основные фонды (средства труда, используемые для производственной деятельности); 
-   оборотные (ОА), используются до года: 
    -   сырье и материалы (запасы, затраты, сырье, материалы и т.п.); 
    -   ДЗ (дебиторская задолженность); 
    -   ДС (денежные средства: остатки средств в кассе и на счетах);

**Пассивы** - источники капитала: 

*   собственный капитал (СК) открытого акционерного общества: определяет  минимальный размер его имущества, гарантирующий интерес его кредиторов;