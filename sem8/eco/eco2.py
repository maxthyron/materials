def f_gpm(p_val, b):
    _gpm = (p_val / b) * 100
    print(f"GPM% = {_gpm}%")
    return _gpm

def f_opm(p_prod, b):
    _opm = (p_prod / b) * 100
    print(f"OPM% = {_opm}%")
    return _opm

def f_z_d(z, b):
    _z_d = (z / b) * 100
    print(f"Z_D% = {_z_d}%")
    return _z_d

def f_kp(kp, b):
    _kp = (kp / b) * 100
    print(f"KP% = {_kp}%")
    return _kp

def f_yp(yp, b):
    _yp = (yp / b) * 100
    print(f"YP% = {_yp}%")
    return _yp

def f_p_proc(p_proc, b):
    _p_proc = (p_proc / b) * 100
    print(f"P_PROC% = {_p_proc}%")
    return _p_proc

def f_d_proch(d_proch, b):
    _d_proch = (d_proch / b) * 100
    print(f"D_PROCH% = {_d_proch}%")
    return _d_proch

def f_p_proch(p_proch, b):
    _p_proch = (p_proch / b) * 100
    print(f"P_PROCH% = {_p_proch}%")
    return _p_proch

YEAR = ["2017", "2016", "2015"]
B = [924095, 932925, 664504]
C = [893530, 898646, 648694]
P_VAL = [30565, 34279, 15811]
KP = [0, 0, 0]
YP = [0, 0, 0]
P_PROD = [30565, 34279, 15811]
P_PROC = [0, 4780, 3235]
D_PROCH = [27249, 19589, 16309]
P_PROCH = [39255, 14210, 22494]
P_DNAL = [27142, 39297, 16391]
NP = [7764, 7956, 4819]
P_CHIST = [20316, 30707, 11276]

for i in range(3):
    print(YEAR[i])
    print()
    f_gpm(P_VAL[i], B[i])
    f_opm(P_PROD[i], B[i])
    f_z_d(C[i], B[i])
    f_kp(KP[i], B[i])
    f_yp(YP[i], B[i])
    f_p_proc(P_PROC[i], B[i])
    f_d_proch(D_PROCH[i], B[i])
    f_p_proch(P_PROCH[i], B[i])
    print()
    print("-" * 100)
    


