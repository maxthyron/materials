def f_cha(a, do, ko):
    _cha = a - do - ko
    print("CHA = ", _cha) 
    return _cha

def f_cok(oa, ko):
    _cok = oa - ko
    print("COK = ", _cok)
    return _cok

def f_pa(osn_sr, zap):
    _pa = (osn_sr + zap) / (10 ** 9)
    print("PA = ", _pa * (10 ** 9))
    print("PA(bil) = ", _pa)
    return _pa

def f_k_pa(pa, a):
    _k_pa = pa / a
    print("K_PA = ", _k_pa)
    return _k_pa

def f_m_ck(ba, oa, ck):
    _m_ck = (ba + oa) / ck
    print("M_CK = ", _m_ck)
    return _m_ck

def f_k_abt(ck, a):
    _k_abt = ck / a
    print("K_ABT = ", _k_abt)
    return _k_abt

def f_k_fin(do, ko, a):
    _k_fin = (do + ko) / a
    print("K_FIN = ", _k_fin)
    return _k_fin

def f_k_man(cok, ck):
    _k_man = cok / ck
    print("K_MAN = ", _k_man)
    return _k_man


def f_k_cok(cok, oa):
    _k_cok = cok / oa
    print("K_COK = ", _k_cok)
    return _k_cok

def f_l_abs(dc, ko):
    _l_abs = dc / ko
    print("L_ABS = ", _l_abs)
    return _l_abs

def f_l_sroch(dc, d3, ko):
    _l_sroch = (dc + d3) / ko
    print("L_SROCH = ", _l_sroch)
    return _l_sroch

def f_l_tek(oa, ko):
    _l_tek = oa / ko
    print("L_TEK = ", _l_tek)
    return _l_tek

YEAR = ["2017", "2016", "2015"]
A = [2503169, 2603473, 2693771]
KO = [348075, 503159, 709600]
DO = [10043, 11552, 34433]
OA = [1044923, 1116155, 1211058]
BA = [1458246, 1487318, 1482713]

CK = [2145051, 2088762, 1949739]
COK = [696848, 612996, 501458]
DC = [167418, 165255, 33194]
D3 = [292923, 216848, 388143]

OCH_SR = [1350377, 1415779, 1469936]
ZAP = [499852, 683659, 788728]

for i in range(3):
    print(YEAR[i])
    print()
    f_cha(A[i], DO[i], KO[i])
    f_cok(OA[i], KO[i])
    pa = f_pa(OCH_SR[i], ZAP[i])
    f_k_pa(pa, A[i]) 
    f_m_ck(BA[i], OA[i], CK[i])
    f_k_abt(CK[i], A[i])
    f_k_fin(DO[i], KO[i], A[i])
    f_k_man(COK[i], CK[i])
    f_k_cok(COK[i], OA[i])
    f_l_abs(DC[i], KO[i])
    f_l_sroch(DC[i], D3[i], KO[i])
    f_l_tek(OA[i], KO[i])
    print("-"*100)
    print()
