from PyQt5 import uic
from PyQt5.QtWidgets import *
from random import random, seed
from Queue import *


class MainWindowApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("window.ui", self)
        self.process_btn.clicked.connect(self.process)

    def generate_mean(self):
        return float(self.a_value.text()) + random() * (
                    float(self.b_value.text()) - float(self.a_value.text()))

    def generate_norm(self):
        sum = 0
        for i in range(12):
            sum += random()
        return abs(float(self.m_value.text()) + float(self.sigma_value.text()) * (sum - 6))

    def dt_principle(self):
        # random.seed()
        current_time = 0
        dt = float(self.dt_value.text())
        time_scale = 0
        process_time_scale = 0
        requests_time = float(self.request_value.text())

        current_request_time = self.generate_norm()
        current_process_time = self.generate_mean()
        # print(dt, current_request_time, current_process_time, requests_time)
        my_queue = Queue(start_size=10)
        #
        while current_time <= requests_time:
            print("(DT) Заполненность: " + str(round(my_queue.fill / my_queue.size, 3) * 100) + "%")
            # print(dt, current_request_time, current_process_time, requests_time, my_queue.size)
            if time_scale >= current_request_time:
                time_scale = 0
                current_request_time = self.generate_norm()
                # собрать статистику
                # проверка на увеличение очереди

                if my_queue.add_request():
                    print()

            if process_time_scale >= current_process_time:
                process_time_scale = 0
                current_process_time = self.generate_mean()
                # собрать статистику

            current_time += dt
            time_scale += dt
            process_time_scale += dt
        self.dt_principle_line.setText(str(my_queue.size))

    def event_principle(self):
        # random.seed()
        current_time = 0
        requests = int(self.request_value.text())
        my_queue = Queue(start_size=10)
        current_request_time = self.generate_norm()
        current_process_time = self.generate_mean()
        # print(current_process_time, current_request_time)
        sbs = [current_request_time, current_process_time, requests]

        while True:
            sbs_min, i_min = min(sbs), sbs.index(min(sbs))
            if i_min == 0:
                current_time = sbs[i_min]
                sbs[i_min] = current_time + self.generate_norm()
                print("(Event) Заполненность: " + str(
                    round(my_queue.fill / my_queue.size, 3) * 100) + "%")
                if my_queue.add_request():
                    print()
                    # self.event_principle_line.setText(str(my_queue.size))
            elif i_min == 1:
                current_time = sbs[i_min]
                sbs[i_min] = current_time + self.generate_mean()
                # сбор статистики
            else:
                self.event_principle_line.setText(str(my_queue.size))
                return

    def process(self):
        QApplication.processEvents()
        self.dt_principle()
        self.event_principle()

        # qs_dt = self.dt_principle()
        # qs_event = self.event_principle()
        # self.dt_principle_line.setText(str(qs_dt))
        # self.event_principle_line.setText(str(qs_event))


seed()
if __name__ == "__main__":
    import sys

    app = QApplication(sys.argv)
    mw = MainWindowApp()
    mw.show()
    sys.exit(app.exec_())
