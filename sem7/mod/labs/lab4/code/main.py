import sys, random
from PyQt5.QtWidgets import QDialog, QApplication, QPushButton, \
    QVBoxLayout, QLineEdit, QLabel
from PyQt5 import QtGui
import numpy.random as nr


class Window(QDialog):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        QApplication.processEvents()
        self.setGeometry(100, 100, 500, 600)

        self.text = QLabel("Генерация заявок")

        self.mu = QLabel("Математическое ожидание")
        self.mu_mssg = QLineEdit()
        self.mu_mssg.setText("10")

        self.d = QLabel("Дисперсия")
        self.d_mssg = QLineEdit()
        self.d_mssg.setText("0.1")

        self.text2 = QLabel("Обслуживание заявок")

        self.a = QLabel("a")
        self.a_mssg = QLineEdit()
        self.a_mssg.setText("0")

        self.b = QLabel("b")
        self.b_mssg = QLineEdit()
        self.b_mssg.setText("10")

        self.p = QLabel("Вероятность отказа")
        self.p_mssg = QLineEdit()
        self.p_mssg.setText("0.1")

        self.dt = QLabel("dt")
        self.dt_mssg = QLineEdit()
        self.dt_mssg.setText("0.01")

        self.requests = QLabel("Количество заявок")
        self.requests_mssg = QLineEdit()
        self.requests_mssg.setText("300")

        self.button = QPushButton('Рассчитать')

        self.event_res = QLabel("Событийный принцип")
        self.event_res_mssg = QLineEdit()

        self.dt_res = QLabel("Принцип dt")
        self.dt_res_mssg = QLineEdit()

        vlayout = QVBoxLayout()

        vlayout.addWidget(self.text)
        vlayout.addWidget(self.mu)
        vlayout.addWidget(self.mu_mssg)
        vlayout.addWidget(self.d)
        vlayout.addWidget(self.d_mssg)
        vlayout.addWidget(self.text)
        vlayout.addWidget(self.text2)
        vlayout.addWidget(self.text)
        vlayout.addWidget(self.a)
        vlayout.addWidget(self.a_mssg)
        vlayout.addWidget(self.b)
        vlayout.addWidget(self.b_mssg)
        vlayout.addWidget(self.text)
        vlayout.addWidget(self.p)
        vlayout.addWidget(self.p_mssg)
        vlayout.addWidget(self.dt)
        vlayout.addWidget(self.dt_mssg)
        vlayout.addWidget(self.text)
        vlayout.addWidget(self.requests)
        vlayout.addWidget(self.requests_mssg)
        vlayout.addWidget(self.text)
        vlayout.addWidget(self.button)
        vlayout.addWidget(self.text)
        vlayout.addWidget(self.event_res)
        vlayout.addWidget(self.event_res_mssg)
        vlayout.addWidget(self.dt_res)
        vlayout.addWidget(self.dt_res_mssg)

        self.setLayout(vlayout)

        self.button.clicked.connect(self.run)

    def run(self):
        QApplication.processEvents()
        mu = float(self.mu_mssg.text())
        d = float(self.d_mssg.text())

        a = float(self.a_mssg.text())
        b = float(self.b_mssg.text())

        p = float(self.p_mssg.text())
        dt = float(self.dt_mssg.text())

        requests = float(self.requests_mssg.text())

        model = Model(dt, requests, p)

        dt_result = model.dt_modelling(a, b, mu, d)
        event_result = model.event_modelling(a, b, mu, d)

        self.event_res_mssg.setText(str(event_result))
        self.dt_res_mssg.setText(str(dt_result))

        return 0


class Model():
    def __init__(self, dt, requests, reenter_prob):
        self.dt = dt
        self.requests = requests
        self.reenter_prob = reenter_prob
        self.queue = 0
        self.queue_max = 0
        self.reenter = 0

    def check_queue_max(self):
        if self.queue > self.queue_max:
            self.queue_max = self.queue

    def add_to_queue(self):
        self.queue += 1
        self.check_queue_max()

    def rem_from_queue(self):
        if self.queue > 0:
            self.queue -= 1

        if nr.uniform(0, 1) < self.reenter_prob:
            self.reenter += 1
            self.add_to_queue()

        return 1

    def event_modelling(self, a, b, mu, d):
        req_done_count = 0
        t_generation = nr.uniform(a, b)
        t_proccessor = t_generation + abs(nr.normal(mu, d))
        i = 0
        while req_done_count < self.requests:
            if t_generation <= t_proccessor:
                if i <= self.requests:
                    self.add_to_queue()

                i += 1
                t_generation += nr.uniform(a, b)
            if t_generation >= t_proccessor:
                req_done_count += self.rem_from_queue()
                t_proccessor += abs(nr.normal(mu, d))
        print('Событийный принцип')
        print('Отказ:', self.reenter)
        print('Время обработки:', t_proccessor * t_generation / (t_proccessor + t_generation * self.reenter_prob))

        return self.queue_max

    def dt_modelling(self, a, b, mu, d):
        req_done_count = 0
        t_generation = nr.uniform(a, b)
        t_proccessor = t_generation + abs(nr.normal(mu, d))
        t_curr = 0
        i = 0
        while req_done_count < self.requests:
            if t_generation <= t_curr:
                if i <= self.requests:
                    self.add_to_queue()
                i += 1
                t_generation += nr.uniform(a, b)
            if t_curr >= t_proccessor:
                req_done_count += self.rem_from_queue()
                t_proccessor += abs(nr.normal(mu, d))
            t_curr += self.dt
        print('Принцип dt')
        print('Отказ:', self.reenter)
        print('Время обработки:', t_proccessor * t_generation / (t_proccessor + t_generation * self.reenter_prob))

        return self.queue_max

# 1/T = 1/Tgen + Potk/Tproc
if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = Window()
    main.show()
    app.exec()

