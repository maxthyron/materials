import numpy
from numpy.linalg.linalg import LinAlgError
import math as m


def build_coeff_matrix(matrix):
    matrix = numpy.array(matrix)
    count = len(matrix)
    res = numpy.zeros((count, count))
    for state in range(count - 1):
        for col in range(count):
            res[state, state] -= matrix[state, col]
        for row in range(count):
            res[state, row] += matrix[row, state]

    for state in range(count):
        res[count - 1, state] = 1
    return res


def build_augmentation_matrix(count):
    res = [0 for _ in range(count)]
    res[count - 1] = 1
    return numpy.array(res)


def get_system_times_s(matrix):
    time_res = []
    try:
        res = numpy.linalg.solve(build_coeff_matrix(matrix), build_augmentation_matrix(len(matrix)))
        for i in range(len(matrix)):
            sum1 = 0
            sum2 = 0
            for j in range(len(matrix[0])):
                sum1 += matrix[i][j]
                sum2 += matrix[j][i]
            time_res.append((sum1 - sum2) / res[i])
    except LinAlgError:
        res = []
    print(res)
    return time_res


def calculate_probabilities(matrix):
    size = len(matrix)
    equations = numpy.zeros((size, size))
    answers = numpy.zeros(size)

    for i in range(size - 1):
        for j in range(size):
            equations[i][j] = matrix[j][i]

        sum = 0
        for j in range(size):
            sum += matrix[i][j]
        equations[i][i] = -sum

    for i in range(size):
        equations[size - 1][i] = 1
    answers[size - 1] = 1
    print(equations, answers)
    return numpy.linalg.solve(equations, answers)


def get_system_times(matrix):
    probs = calculate_probabilities(matrix)
    times = []
    # for i in range(len(matrix)):
    #     sum = 0
    #     for j in range(len(matrix[i])):
    #         sum += matrix[i][j]
    #     times.append(probs[i] / sum)
    print(matrix)

    for i in range(len(matrix)):
        sum1 = 0
        sum2 = 0
        for j in range(len(matrix[i])):
            sum1 += matrix[i][j]
            sum2 += matrix[j][i]
        times.append(sum2 / probs[i])
    return times
