#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    srand(time(nullptr));
    ui->setupUi(this);

    createModel(&model_table, ui->t_table, UI_TABLE_ROW, UI_TABLE_COL);
    createModel(&model_rand, ui->t_rand, UI_TABLE_ROW, UI_TABLE_COL);
    createModel(&model_manual, ui->t_manual, 0, 1);

    createModel(&model_table_estimate, ui->t_estimate_table, 1, UI_TABLE_COL);
    createModel(&model_rand_estimate, ui->t_estimate_rand, 1, UI_TABLE_COL);
    createModel(&model_manual_estimate, ui->t_estimate_manual, 0, 1);

    IntMatrix fileTable;
    IntMatrix randTable;

    fill_randTable_from_file(&fileTable, "../../../../code/table.txt");
    fill_randTable_with_generator(&randTable);

    vector<QString> tableTable_estimate = test_randomness(&fileTable);
    vector<QString> randTable_estimate = test_randomness(&randTable);

    fillModel(model_table, &fileTable);
    fillModel(model_rand, &randTable);

    fillModelEstimate(model_table_estimate, &tableTable_estimate);
    fillModelEstimate(model_rand_estimate, &randTable_estimate);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::resizeTable(QTableView *table)
{
    for (int c = 0; c < table->horizontalHeader()->count(); ++c)
    {
        table->horizontalHeader()->setSectionResizeMode(
            c, QHeaderView::Stretch);
    }
    for (int c = 0; c < table->verticalHeader()->count(); ++c)
    {
        table->verticalHeader()->setSectionResizeMode(
            c, QHeaderView::Stretch);
    }

}

void MainWindow::resizeEvent(QResizeEvent *event) {
    resizeTable(ui->t_table);
    resizeTable(ui->t_rand);
    resizeTable(ui->t_manual);

    resizeTable(ui->t_estimate_table);
    resizeTable(ui->t_estimate_rand);
    resizeTable(ui->t_estimate_manual);

    QMainWindow::resizeEvent(event);
}

void MainWindow::fillModel(QStandardItemModel *model, IntMatrix *m)
{

    for(int row=0; row!=model->rowCount(); ++row){
        for(int col=0; col!=model->columnCount(); ++col) {
            QStandardItem *newItem = new QStandardItem(tr("%1").arg(m->at(size_t(col))[size_t(row)]));
            newItem->setData(Qt::AlignCenter, Qt::TextAlignmentRole);
            model->setItem(row, col, newItem);
        }
    }
}

void MainWindow::fillModelEstimate(QStandardItemModel *model, vector<QString> *m)
{

    for(int col=0; col!=model->columnCount(); ++col){
        QStandardItem *newItem = new QStandardItem(tr("%1").arg(m->at(size_t(col))));
        newItem->setData(Qt::AlignCenter, Qt::TextAlignmentRole);
        model->setItem(0, col, newItem);
    }
}

void MainWindow::createModel(QStandardItemModel **model, QTableView *vtable, int row, int col)
{
    *model = new QStandardItemModel(row, col, vtable);

    if (col == 1)
        (*model)->setHeaderData(0, Qt::Horizontal, QObject::tr("Введенные данные"));
    else
        (*model)->setHeaderData(0, Qt::Horizontal, QObject::tr("0-9"));
    (*model)->setHeaderData(1, Qt::Horizontal, QObject::tr("10-99"));
    (*model)->setHeaderData(2, Qt::Horizontal, QObject::tr("100-999"));

    vtable->setModel(*model);
}

void MainWindow::on_actionExit_triggered()
{
    QCoreApplication::quit();
}

void MainWindow::resizeModelManual(QStandardItemModel *model, size_t row)
{
    int dist = model->rowCount() - int(row);

    if(!dist)
        return;
    else if(dist > 0)
        model->removeRows(0, dist);
    else
        model->insertRows(0, abs(dist));
}

void MainWindow::fillModelManual(QStandardItemModel *model, vector<int> *m)
{
    for(int row=0; row!=model->rowCount(); ++row){
        QStandardItem *newItem = new QStandardItem(tr("%1").arg(m->at(size_t(row))));
        newItem->setData(Qt::AlignCenter, Qt::TextAlignmentRole);
        model->setItem(row, 0, newItem);
    }
}

void MainWindow::on_actionInput_triggered()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr(""),
                     tr("Введите ваши значения(через пробел):"), QLineEdit::Normal, "", &ok);
    if (!ok && text.isEmpty())
        return;

    QStringList strValues;
    strValues = text.split(QRegExp("\\s+"), QString::SkipEmptyParts);

    vector<int> values;
    for(int i = 0; i < strValues.size(); i++) {
        values.push_back(strValues[i].toInt());
    }

    resizeModelManual(this->model_manual, values.size());
    fillModelManual(this->model_manual, &values);

    float z = test_column_randomness(&values);

    QString test_res;
    if(fabs(z) > CRIT_VALUE)
        test_res.sprintf("%.4f>%.4f\nH0 не верна", double(fabs(z)), double(CRIT_VALUE));
    else
        test_res.sprintf("%.4f>%.4f\nH0 верна", double(fabs(z)), double(CRIT_VALUE));
    vector<QString> test_res_wrap;
    test_res_wrap.push_back(test_res);

    fillModelEstimate(model_manual_estimate, &test_res_wrap);

    emit resizeEvent(nullptr);
}

void MainWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Октрыть файл"), "../../../../code", tr("Image Files (*.txt)"));

    IntMatrix table;
    fill_randTable_from_file(&table, fileName);

    vector<QString> estimate = test_randomness(&table);

    fillModel(model_table, &table);
    fillModelEstimate(model_table_estimate, &estimate);

    emit resizeEvent(nullptr);
}

void MainWindow::on_buttonOpen_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Октрыть файл"), "../../../../code", tr("Image Files (*.txt)"));
    if (fileName == nullptr) {
        qDebug() << "Файл не выбран" << endl;
        return;
    }

    IntMatrix table;
    fill_randTable_from_file(&table, fileName);

    vector<QString> estimate = test_randomness(&table);

    fillModel(model_table, &table);
    fillModelEstimate(model_table_estimate, &estimate);

    emit resizeEvent(nullptr);
}

void MainWindow::on_buttonInput_clicked()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr(""),
                     tr("Введите ваши значения(через пробел):"), QLineEdit::Normal, "", &ok);
    if (!ok && text.isEmpty())
        return;

    QStringList strValues;
    strValues = text.split(QRegExp("\\s+"), QString::SkipEmptyParts);

    vector<int> values;
    for(int i = 0; i < strValues.size(); i++) {
        values.push_back(strValues[i].toInt());
    }

    resizeModelManual(this->model_manual, values.size());
    fillModelManual(this->model_manual, &values);

    qDebug() << "Input data:" << values << endl;
    float p = test_column_randomness(&values);

    QString test_res;
    test_res.sprintf("%.2f%%", double(p));
//    if(fabs(p) > CRIT_VALUE)
//        test_res.sprintf("%.4f>%.4f\nH0 не верна", double(fabs(p)), double(CRIT_VALUE));
//    else
//        test_res.sprintf("%.4f>%.4f\nH0 верна", double(fabs(p)), double(CRIT_VALUE));
    vector<QString> test_res_wrap;
    test_res_wrap.push_back(test_res);

    fillModelEstimate(model_manual_estimate, &test_res_wrap);

    emit resizeEvent(nullptr);
}
