#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QResizeEvent>
#include <QStandardItemModel>
#include <QHeaderView>
#include <QStandardItem>
#include <QTableView>
#include <QCoreApplication>
#include <QDialog>
#include <QInputDialog>

#include "pseudo_numbers.h"

#define UI_TABLE_ROW_MANUAL 50
#define UI_TABLE_ROW 10
#define UI_TABLE_COL 3

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

protected:
    void resizeEvent(QResizeEvent *event) override;
    void resizeTable(QTableView *table);

    void fillModel(QStandardItemModel *model, IntMatrix *m);
    void fillModelEstimate(QStandardItemModel *model, vector<QString> *m);
    void createModel(QStandardItemModel **model, QTableView *table, int row, int col);

    void fillModelManual(QStandardItemModel *model, vector<int> *m);
    void resizeModelManual(QStandardItemModel *model, size_t row);

private slots:
    void on_actionExit_triggered();

    void on_actionInput_triggered();

    void on_actionOpen_triggered();

    void on_buttonOpen_clicked();

    void on_buttonInput_clicked();

private:
    Ui::MainWindow *ui;
    QStandardItemModel *model_table;
    QStandardItemModel *model_rand;
    QStandardItemModel *model_manual;

    QStandardItemModel *model_table_estimate;
    QStandardItemModel *model_rand_estimate;
    QStandardItemModel *model_manual_estimate;
};

#endif // MAINWINDOW_H
