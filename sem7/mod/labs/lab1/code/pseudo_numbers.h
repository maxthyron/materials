#ifndef PSEUDO_NUMBERS_H
#define PSEUDO_NUMBERS_H

#define SHOWN_NUMS 10
#define TABLE_ROW 100
#define TABLE_COL 3
#define CRIT_VALUE 1.96f

#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cmath>

#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QObject>
#include <QFileDialog>

using namespace std;
typedef vector<vector<int>> IntMatrix;

void test_data(vector<int> *data);

void fill_randTable_from_file(IntMatrix *randTable, const QString filename);
void fill_randTable_with_generator(IntMatrix *randTable);
void fill_randTable_with_manual(IntMatrix *randTable, int *input);

float runs_test(vector<int> *m);
float correlation(vector<int> *m);

float test_column_randomness(vector<int> *m);
vector<QString> test_randomness(IntMatrix *m);

#endif // PSEUDO_NUMBERS_H
