#include "pseudo_numbers.h"

vector<int> get_numbers_from_line(QString line, int column)
{
    QStringList nums = line.split(' ');
    vector<int> row;
    if(nums[0] != "") {
        int number = nums[column].toInt();

        row.push_back(number % 9 + 1);
        row.push_back(number % 90 + 10);
        row.push_back(number % 900 + 100);

    }

    return row;
}

vector<int> get_numbers_from_generator(size_t size)
{
    vector<int> row;

    int digit = 1;
    for(size_t i = 0; i < size; i++) {
        row.push_back(rand() % (digit*9) + digit);
        digit *= 10;
    }

    return row;
}

void fill_randTable_from_file(IntMatrix *randTable, const QString filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "Can't open file: " << file.fileName() << endl;
        return;
    }

    QTextStream in(&file);

    vector<int> column1;
    vector<int> column2;
    vector<int> column3;

    int count = 0;
    while (!in.atEnd()) {
        QString line = in.readLine();
        vector<int> row = get_numbers_from_line(line, 0);

        column1.push_back(row[0]);
        column2.push_back(row[1]);
        column3.push_back(row[2]);

        count++;
        if(count > TABLE_ROW)
            break;
    }

    randTable->push_back(column1);
    randTable->push_back(column2);
    randTable->push_back(column3);
}

void fill_randTable_with_generator(IntMatrix *randTable)
{
    vector<int> column1;
    vector<int> column2;
    vector<int> column3;

    for(size_t i = 0; i < TABLE_ROW; i++) {
        vector<int> row = get_numbers_from_generator(3);

        column1.push_back(row[0]);
        column2.push_back(row[1]);
        column3.push_back(row[2]);
    }

    randTable->push_back(column1);
    randTable->push_back(column2);
    randTable->push_back(column3);
}

void find_median(vector<int> *m, int *median)
{
    vector<int> s = vector<int>(*m);
    sort(s.begin(), s.end());

    *median = s[s.size()/2];
}

float runs_test(vector<int> *m) {
    int median = 0;
    find_median(m, &median);

    int n1 = 0,											// n1 - positive values
        n2 = 0;											// n2 - negative
    int r = 0;                                          // r - observed number of runs
    bool state; // Negative or Positive
    if (m->at(0) < median) {
       state = 0; // Negative
    }
    else {
       state = 1; // Positive
    }

    for(size_t i = 0; i < m->size(); i++) {
        if(m->at(i) < median)
            n2 += 1;
        else if(m->at(i) > median)
            n1 += 1;

        if ((m->at(i) < median && state) || (m->at(i) >= median && !state) ) {
            state = !state;
            r++;
        }
    }
    r++;

    if (n1 == 0 && n2 == 0)
        return nanf("");

    float r_mean = (2 * n1 * n2) / (n1 + n2) + 1;       // r mean
    float s_r = float((2 * n1 * n2 * (2 * n1 * n2 - n1 - n2))) / float(((pow(n1 + n2, 2) * (n1 + n2 - 1))));
    float z = float((r - r_mean)) / float(sqrt(s_r));

    qDebug() << "R: " << r << endl;
    qDebug() << "R Mean: " << r_mean << endl;
    qDebug() << "Sr: " << s_r << endl;
    qDebug() << "Z: " << z << endl;

    return z;
}

float test_column_randomness(vector<int> *m)
{
    float runs = runs_test(m);
    float result_1;
    if (isnan(runs))
    {
        result_1 = 0;
    }

    result_1 = 100.0f - (fabs(runs) / CRIT_VALUE) * 100;


    float corel = fabs(1 - correlation(m));
    float result_2 = corel * 100;

    return 0.5 * result_1 + 0.5 * result_2;
}

float correlation(vector<int> *m)
{
    size_t n = m->size();

    if (n == 0) {
        return 0;
    }

    int seqSum = 0;
    for (size_t i = 0; i < n; ++i)
    {
        seqSum += m->at(i);
    }

    int sumUU = 0;
    int sumU2 = 0;
    for (size_t i = 0; i < n; ++i)
    {
        int numj = m->at((i+1) % n);
        int numi = m->at(i);
        sumU2 += numi * numi;
        sumUU += numi * numj;
    }

    int top = (int(n) * sumUU) - (seqSum * seqSum);
    int bottom = (int(n) * sumU2) - (seqSum * seqSum);

    if (bottom == 0)
        return 1;

    return top / bottom;
}

vector<QString> test_randomness(IntMatrix *m)
{
    vector<float> results;

    results.push_back(test_column_randomness(&m->at(0)));
    results.push_back(test_column_randomness(&m->at(1)));
    results.push_back(test_column_randomness(&m->at(2)));

    vector<QString> testTable;

    for(size_t i = 0; i < results.size(); i++) {
        QString test_res;
        test_res.sprintf("%.2f%%", double(results[i]));
//        if(fabs(results[i]) > CRIT_VALUE)
//            test_res.sprintf("%.4f>%.4f\nH0 не верна", double(fabs(results[i])), double(CRIT_VALUE));
//        else
//            test_res.sprintf("%.4f>%.4f\nH0 верна", double(fabs(results[i])), double(CRIT_VALUE));

        testTable.push_back(test_res);
    }


    return testTable;
}
