from algorithms import Algorithms
import struct
import sys


class RSA:
    def __init__(self, bits):
        self.bits = bits
        self.e = 0x10001  # Fast exponent operator
        self.p, self.q = self.get_p_q()
        self.n = self.p * self.q
        self.phi = (self.p - 1) * (self.q - 1)
        self.d = Algorithms.find_d(self.phi, self.e)

    def get_p_q(self):
        while True:
            p = Algorithms.find_prime(self.bits, self.e)
            q = Algorithms.find_prime(self.bits, self.e)
            if p != q:
                break
        return p, q

    def encrypt(self, finput, foutput):
        with open(finput, "rb") as fi, open(foutput, "w") as fo:
            data = fi.read()
            for item in data:
                new_item = pow(item, self.e, self.n)
                fo.write(str(new_item) + "\n")
        print("Encrypted")

    def decrypt(self, finput, foutput):
        with open(finput, "r") as fi, open(foutput, "wb") as fo:
            line = fi.readline()
            while line:
                num = int(line)
                byte = pow(num, self.d, self.n)
                fo.write(struct.pack('B', byte))
                line = fi.readline()
        print("Decrypted")


def main():
    if len(sys.argv) < 2:
        filename = input("Enter filename: ")
    else:
        filename = sys.argv[1]
    rsa = RSA(256)
    rsa.encrypt(filename, "enc_" + filename)
    rsa.decrypt("enc_" + filename, "dec_" + filename)


if __name__ == "__main__":
    main()
