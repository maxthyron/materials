import random
import math


class Algorithms:
    @staticmethod
    def find_prime(bits, e):
        p = random.getrandbits(bits // 2)
        rounds = int(math.log(p, 2))
        while True:  # Find p
            p |= (1 << (bits // 2 - 2)) | (1 << 0)
            if Algorithms.miller_rabin(p, rounds) and Algorithms.gcd(e, p - 1) == 1:
                break
            else:
                p += 2

        return p

    @staticmethod
    def miller_rabin(n, rounds):
        # n - 1 = 2^s * t
        t = n - 1
        s = 0
        while t % 2 == 0:
            t >>= 1
            s += 1

        for _ in range(rounds):
            a = random.randrange(2, n - 2)

            x = pow(a, t, n)
            if x == 1 or x == n - 1:
                return True
            for _ in range(s - 1):
                x = pow(x, 2, n)
                if x == 1:
                    return False
                if x == n - 1:
                    return True
            return False
        return True

    @staticmethod
    def gcd(a, b):
        if a < b:
            a, b = b, a
        while b:
            a, b = b, a % b
        return a

    @staticmethod
    def advanced_euclid(a, b):
        x2, x1, y2, y1 = 1, 0, 0, 1
        while b > 0:
            q = a // b
            r = a - q * b

            x = x2 - q * x1
            y = y2 - q * y1
            x2, x1, y2, y1 = x1, x, y1, y

            a = b
            b = r

        d = a
        y = y2

        return d, y

    @staticmethod
    def find_d(phi, e):
        while True:
            # находим d такое, чтобы (e * d) mod phi = 1
            gcd, d = Algorithms.advanced_euclid(phi, e)
            if gcd == 1:
                break

        if d < 0:
            d = phi + d
        return d
