import {getHTML, parseScheduleHTML} from "./utils.mjs"

function getGroups(links) {
    let groups = [];
    links.forEach((groupLink) => {
        let uri = groupLink.getAttribute("href");
        let group = groupLink.innerHTML.trim();
        groups.push({group: group, uri: uri})
    });

    return groups;
}

export function getGroupsList(url) {
    let html = getHTML(url);
    let links = parseScheduleHTML(html);
    let groupsList = getGroups(links);

    return groupsList;
}