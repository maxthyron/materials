import {getGroupsList} from './groups.mjs'
import {baseUrl, scheduleUrl} from "./config.mjs"
import {getHTML, parseWeekHTML, getFileHTML} from "./utils.mjs";


export function getWeekSubjects(uri) {
    let weekHTML = getHTML(baseUrl + uri);
    let groupSubjects = parseWeekHTML(weekHTML);

    return groupSubjects;
}

export function main() {
    let groups = getGroupsList(baseUrl + scheduleUrl);

    let subjectsObject = {type: "occupied", subjects: []};
    groups.forEach((group) => {
        console.log(group.group);
        let groupWeekSubjects = getWeekSubjects(group.uri);
        subjectsObject.subjects.push(groupWeekSubjects);
    });

    console.log(subjectsObject);
}
