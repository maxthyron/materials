import XMLHttpRequest from "xmlhttprequest";
import jsdom from "jsdom";
import fs from "fs";

const {JSDOM} = jsdom;

export function getHTML(url) {
    let xmlHttp = new XMLHttpRequest.XMLHttpRequest();
    xmlHttp.open("GET", url, false);
    xmlHttp.send();

    return xmlHttp.responseText;
}

export function getFileHTML(url) {
    let path = process.cwd();
    let buffer = fs.readFileSync(path + "/schedule.html");

    return buffer.toString();
}

export function parseScheduleHTML(html) {
    const dom = new JSDOM(html);
    let links = dom.window.document.querySelectorAll("a.btn, a.btn-sm, a.btn-default, a.text-nowrap");

    return links;
}

function getSubject(cell) {
    let data = cell.children;

    let subject = {};
    if (data.length) {
        subject.type = data[0].textContent;
        subject.name = data[1].textContent;
        if (data[2].textContent === "") {
            return null;
        }
        subject.audit = data[2].textContent;
        subject.professor = data[3].textContent;

        return subject;
    }

    return null;
}

export function parseWeekHTML(html) {
    const dom = new JSDOM(html);
    let days = dom.window.document.querySelectorAll("div.col-md-6.hidden-xs > table");

    let daysObject = {days: []};
    days.forEach((day) => {
        let dayShortName = day.getElementsByTagName("td")[0].textContent;
        let times = day.getElementsByTagName("tr");
        let timesObject = [];
        for (let i = 2; i < times.length; ++i) {
            let cells = times[i].cells;
            let time = cells[0].textContent;
            let subject = getSubject(cells[1]);

            let subjectSecond = null;
            if (cells.length > 2) {
                subjectSecond = getSubject(cells[2]);
            }
            let subjects = [];
            if (subject || subjectSecond) {
                if (subject) {
                    subjects.push(subject);
                }
                if (subjectSecond) {
                    subjects.push(subjectSecond);
                }
            }
            if (subjects.length) {
                let timeObject = {time: time, subjects: subjects};
                timesObject.push(timeObject);
            }
        }
        if (timesObject.length) {
            let dayObject = {day: dayShortName, times: timesObject};
            daysObject.days.push(dayObject);
        }
    });
}