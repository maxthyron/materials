# Лабораторная работа 2

## Задание 1: 

*   ### Замер скорости отдачи контента на сервере из лабораторной работы 1. 

*   ### Логирование приходящих запросов.

### Замер скорости:

-   ##### GET запрос с картинкой

    ```bash
    $ ab -c 40 -n 400 http://127.0.0.1:3000/
    
    Server Software:        
    Server Hostname:        127.0.0.1
    Server Port:            3000
    
    Document Path:          /
    Document Length:        367 bytes
    
    Concurrency Level:      40
    Time taken for tests:   0.199 seconds
    Complete requests:      400
    Failed requests:        0
    Total transferred:      262000 bytes
    HTML transferred:       146800 bytes
    Requests per second:    2014.24 [#/sec] (mean)
    Time per request:       19.859 [ms] (mean)
    Time per request:       0.496 [ms] (mean, across all concurrent requests)
    Transfer rate:          1288.41 [Kbytes/sec] received
    
    Connection Times (ms)
    min  mean[+/-sd] median   max
    Connect:        0    1   0.8      0       6
    Processing:     2   19   3.4     19      27
    Waiting:        1   17   3.2     16      25
    Total:          2   19   3.2     20      27
    WARNING: The median and mean for the initial connection time are not within a normal deviation
    These results are probably not that reliable.
    
    Percentage of the requests served within a certain time (ms)
    50%     20
    66%     21
    75%     21
    80%     22
    90%     24
    95%     24
    98%     25
    99%     26
    100%     27 (longest request)
    ```

-   ##### GET запрос на /hack

    ```bash
    $ ab -c 40 -n 400 http://127.0.0.1:3000/hack
    
    Server Software:        
    Server Hostname:        127.0.0.1
    Server Port:            3000
    
    Document Path:          /hack
    Document Length:        28 bytes
    
    Concurrency Level:      40
    Time taken for tests:   0.185 seconds
    Complete requests:      400
    Failed requests:        0
    Total transferred:      125600 bytes
    HTML transferred:       11200 bytes
    Requests per second:    2157.21 [#/sec] (mean)
    Time per request:       18.543 [ms] (mean)
    Time per request:       0.464 [ms] (mean, across all concurrent requests)
    Transfer rate:          661.49 [Kbytes/sec] received
    
    Connection Times (ms)
                  min  mean[+/-sd] median   max
    Connect:        0    1   0.4      0       2
    Processing:     3   17   4.2     16      25
    Waiting:        2   15   4.0     14      25
    Total:          4   18   4.3     17      26
    ERROR: The median and mean for the initial connection time are more than twice the standard
           deviation apart. These results are NOT reliable.
    
    Percentage of the requests served within a certain time (ms)
      50%     17
      66%     18
      75%     21
      80%     22
      90%     25
      95%     26
      98%     26
      99%     26
     100%     26 (longest request)
    ```

-   ##### GET запрос к API

    ```bash
    $ ab -c 40 -n 400 http://127.0.0.1:3000/warships
    
    Server Software:        
    Server Hostname:        127.0.0.1
    Server Port:            3000
    
    Document Path:          /warships
    Document Length:        421 bytes
    
    Concurrency Level:      40
    Time taken for tests:   0.212 seconds
    Complete requests:      400
    Failed requests:        0
    Total transferred:      252000 bytes
    HTML transferred:       168400 bytes
    Requests per second:    1887.47 [#/sec] (mean)
    Time per request:       21.192 [ms] (mean)
    Time per request:       0.530 [ms] (mean, across all concurrent requests)
    Transfer rate:          1161.24 [Kbytes/sec] received
    
    Connection Times (ms)
                  min  mean[+/-sd] median   max
    Connect:        0    0   0.2      0       1
    Processing:     7   20   5.4     19      32
    Waiting:        3   12   5.5     11      30
    Total:          7   20   5.5     20      32
    
    Percentage of the requests served within a certain time (ms)
      50%     20
      66%     21
      75%     22
      80%     22
      90%     32
      95%     32
      98%     32
      99%     32
     100%     32 (longest request)
    ```

*   ##### PUT запрос к API

    ```bash
    $ ab -c 40 -n 400 -u 'static/json/request.json' -T application/json http://127.0.0.1:3000/warships
    
    Server Software:        
    Server Hostname:        127.0.0.1
    Server Port:            3000
    
    Document Path:          /warships
    Document Length:        7 bytes
    
    Concurrency Level:      40
    Time taken for tests:   0.121 seconds
    Complete requests:      400
    Failed requests:        0
    Total transferred:      82000 bytes
    Total body sent:        65600
    HTML transferred:       2800 bytes
    Requests per second:    3304.56 [#/sec] (mean)
    Time per request:       12.104 [ms] (mean)
    Time per request:       0.303 [ms] (mean, across all concurrent requests)
    Transfer rate:          661.56 [Kbytes/sec] received
                            529.25 kb/s sent
                            1190.80 kb/s total
    
    Connection Times (ms)
                  min  mean[+/-sd] median   max
    Connect:        0    0   0.2      0       1
    Processing:     2   11   2.3     11      16
    Waiting:        1    7   2.5      7      14
    Total:          2   12   2.3     11      16
    
    Percentage of the requests served within a certain time (ms)
      50%     11
      66%     12
      75%     14
      80%     14
      90%     16
      95%     16
      98%     16
      99%     16
     100%     16 (longest request)
    ```

*   POST запрос к API

    ```bash
    $ ab -c 40 -n 400 -p 'static/json/request.json' -T application/json http://127.0.0.1:3000/warships
    
    Server Software:        
    Server Hostname:        127.0.0.1
    Server Port:            3000
    
    Document Path:          /warships
    Document Length:        16 bytes
    
    Concurrency Level:      40
    Time taken for tests:   0.130 seconds
    Complete requests:      400
    Failed requests:        0
    Total transferred:      86400 bytes
    Total body sent:        66000
    HTML transferred:       6400 bytes
    Requests per second:    3070.62 [#/sec] (mean)
    Time per request:       13.027 [ms] (mean)
    Time per request:       0.326 [ms] (mean, across all concurrent requests)
    Transfer rate:          647.71 [Kbytes/sec] received
                            494.78 kb/s sent
                            1142.49 kb/s total
    
    Connection Times (ms)
                  min  mean[+/-sd] median   max
    Connect:        0    0   0.3      0       2
    Processing:     2   12   2.9     12      18
    Waiting:        1    7   3.0      7      15
    Total:          2   13   2.9     12      18
    
    Percentage of the requests served within a certain time (ms)
      50%     12
      66%     14
      75%     15
      80%     15
      90%     17
      95%     17
      98%     18
      99%     18
     100%     18 (longest request)
    ```

### Логи:

#### 	Использование пакета express-logging:

```javascript
var express = require('express');
var logger = require('logops');
var expressLogging = require('express-logging');


var app = express();
app.use(expressLogging(logger));
```

## Задание 2:

-   ### Перенаправление запросов с nginx на сервер первой лабораторной работы.

Сервер первой первой лабораторной работы запущен на порту 3000. Конфигурация nginx для перенаправления:

```nginx
    server { # Redirect to lab1 app
        listen 5000;

        access_log  .../lab2/logs/lab1-access.log upstream_time;
        error_log .../lab2/logs/lab1-error.log warn;

        location / {
            proxy_pass http://127.0.0.1:3000;
        }
    }
```

Таким образом при переходе на 127.0.0.1:5000(nginx сервер) запрос будет перенаправлен на 127.0.0.1:3000(node сервер лабораторной 1).

## Задание 3:

*   ### Использование nginx для отдачи статики.

*   ### Измерение времени ответа сервера.

#### Конфигурация nginx сервера для отдачи статики:

```nginx
server { # Static from nginx
    listen 5001;
    server_name localhost 127.0.0.1;

    access_log  .../lab2/logs/static-access.log upstream_time;
    error_log .../lab2/logs/static-error.log warn;

    location / {
        root /Users/thyron/Documents/GitLab/iu7-web-2019-karsakov-maxim/lab1;
        index /static/html/index.html;
    }

    location /img/ {
        root /Users/thyron/Documents/GitLab/iu7-web-2019-karsakov-maxim/lab1/static;
    }

    location /static/ {
        root /Users/thyron/Documents/GitLab/iu7-web-2019-karsakov-maxim/lab1;
    }
}
```

При переходе на 127.0.0.1:5001 будет отображена статика лабораторной 1.

#### Замер времени:

```bash
$ ab -c 40 -n 400 http://127.0.0.1:5001/

Server Software:        nginx/1.17.3
Server Hostname:        127.0.0.1
Server Port:            5001

Document Path:          /
Document Length:        367 bytes

Concurrency Level:      40
Time taken for tests:   0.052 seconds
Complete requests:      400
Failed requests:        0
Total transferred:      240000 bytes
HTML transferred:       146800 bytes
Requests per second:    7713.07 [#/sec] (mean)
Time per request:       5.186 [ms] (mean)
Time per request:       0.130 [ms] (mean, across all concurrent requests)
Transfer rate:          4519.38 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    2   1.3      2       7
Processing:     1    3   1.8      2       8
Waiting:        0    3   1.7      2       8
Total:          2    5   2.1      4       9

Percentage of the requests served within a certain time (ms)
  50%      4
  66%      5
  75%      7
  80%      8
  90%      8
  95%      9
  98%      9
  99%      9
 100%      9 (longest request)
```

##### Итог: максимальное время сократилось в 3 раза(обычный сервер - 27 ms, nginx - 9 ms).

## Задание 4:

*   ### Кеширование и gzip сжатие файлов.

*   ### Измерение скорости времени ответа сервера.

#### Кеширование:

Настраиваем кеширующий сервер на порте 5002, который будет перенаправлять запросы на статический сервер nginx и добавляем к нему gzip сжатие:

```nginx
http {
    ...
    proxy_cache_path .../lab2/nginx-cache levels=1:2 keys_zone=all:32m max_size=1g;
	...

    server { # Cache server
        listen 5002;

        gzip on;
        gzip_comp_level 5;
        gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;

        access_log  .../lab2/logs/cache-access.log upstream_time;
        error_log .../lab2/logs/cache-error.log warn;

        location / {
            proxy_pass http://127.0.0.1:5001/;
            proxy_cache all;
            proxy_cache_valid 404 502 503 1m;
            proxy_cache_valid any 1h;
        }
    }
}
```

#### Замер времени:

```bash
$ ab -c 40 -n 400 http://127.0.0.1:5002/

Server Software:        nginx/1.17.3
Server Hostname:        127.0.0.1
Server Port:            5002

Document Path:          /
Document Length:        367 bytes

Concurrency Level:      40
Time taken for tests:   0.040 seconds
Complete requests:      400
Failed requests:        0
Total transferred:      240000 bytes
HTML transferred:       146800 bytes
Requests per second:    9960.90 [#/sec] (mean)
Time per request:       4.016 [ms] (mean)
Time per request:       0.100 [ms] (mean, across all concurrent requests)
Transfer rate:          5836.47 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    1   0.7      1       4
Processing:     1    3   1.1      3       6
Waiting:        1    3   1.1      2       6
Total:          2    4   1.0      3       7

Percentage of the requests served within a certain time (ms)
  50%      3
  66%      4
  75%      4
  80%      4
  90%      5
  95%      6
  98%      6
  99%      6
 100%      7 (longest request)
```

#### Замена статического nginx сервера на сервер из первой лабораторной:

##### Первый замер:

```bash
Server Software:        nginx/1.17.3
Server Hostname:        127.0.0.1
Server Port:            5002

Document Path:          /
Document Length:        367 bytes

Concurrency Level:      40
Time taken for tests:   0.269 seconds
Complete requests:      400
Failed requests:        0
Total transferred:      270800 bytes
HTML transferred:       146800 bytes
Requests per second:    1486.25 [#/sec] (mean)
Time per request:       26.913 [ms] (mean)
Time per request:       0.673 [ms] (mean, across all concurrent requests)
Transfer rate:          982.61 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.2      0       1
Processing:     6   25   7.0     25      43
Waiting:        4   25   7.0     25      43
Total:          6   26   7.1     25      43

Percentage of the requests served within a certain time (ms)
  50%     25
  66%     29
  75%     30
  80%     32
  90%     36
  95%     39
  98%     41
  99%     42
 100%     43 (longest request)
```



##### N-ный замер:

```bash
Server Software:        nginx/1.17.3
Server Hostname:        127.0.0.1
Server Port:            5002

Document Path:          /
Document Length:        367 bytes

Concurrency Level:      40
Time taken for tests:   0.947 seconds
Complete requests:      400
Failed requests:        0
Total transferred:      270800 bytes
HTML transferred:       146800 bytes
Requests per second:    422.44 [#/sec] (mean)
Time per request:       94.689 [ms] (mean)
Time per request:       2.367 [ms] (mean, across all concurrent requests)
Transfer rate:          279.29 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.2      0       1
Processing:     2   12   3.3     11      20
Waiting:        1   12   3.3     11      20
Total:          3   12   3.2     11      20

Percentage of the requests served within a certain time (ms)
  50%     11
  66%     13
  75%     14
  80%     15
  90%     17
  95%     19
  98%     19
  99%     20
 100%     20 (longest request)
```

##### Итог: Небольшое ускорение ответа для статического сервера. В случае обычного сервера - при первом обращении скорость ухудшилась почти в 2 раза, однако, после нескольких итерации было получено улучшении скорости в ~20%.

## Задание 5:

*   ### Балансировка между 3 запущенными node серверами первой лабораторной.

C помощью утилиты pm2 запущен кластер из 3 серверов первой лабораторной работы. Конфиг:

```javascript
module.exports = {
  apps : [{
    name: "app",
    script: "./../lab1/app.js",

    env_production: {
      NODE_ENV: "production",
    },

    instances: 3,
    exec_mode: "cluster",
    watch: true,
    increment_var : 'PORT',

    env: {
      "PORT": 3000,
      "NODE_ENV": "development"
    }
  }]
};
```

Настройка балансировки:

```nginx
server { # Cluster
    listen 5003;

    access_log  .../lab2/logs/cluster-access.log upstream_time;
    error_log .../lab2/logs/cluster-error.log warn;

    location / {
        proxy_pass  http://backend;
    }
}

upstream backend  {
    server 127.0.0.1:3000 weight=3;
    server 127.0.0.1:3001 weight=1;
    server 127.0.0.1:3002 backup;
}
```

## Задание 6:

*   ### Два сервера с GET запросами

*   ### Перенаправление на них запросов

Сервер:

```javascript
const express = require("express");

let app = express();

app.use(express.static(__dirname));
app.use(express.json());

let port = process.env.PORT || 3003;

app.get('/', (req, res) => {
    console.log('Server #1!');
    res.sendFile(__dirname + '/static/html/index1.html');
});

app.get('/temp', (req, res) => {
    res.sendFile(__dirname + '/static/html/temp1.html');
});

app.listen(port, () => {
    console.log('Server #1 on ' + port);
});
```

Конфигурация nginx:

```nginx
http {
    ...
    server {
        ...
        location /service[12]/static/ { # Serving static for specific server
            alias .../lab2/static/;
        }
        location /service1/ { # Server 1
            proxy_pass http://127.0.0.1:3003/;
        }

        location /service2/ { # Server 2
            proxy_pass http://127.0.0.1:3004/;
        }
    }
}
```

## Задание 7:

*   ### Страница с состоянием сервера

Для этого к конфигурации nginx в каждый раздел server добавлено следующее:

```nginx
location /basic_status {
	stub_status;
}
```

